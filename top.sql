-- phpMyAdmin SQL Dump
-- version 4.0.10.6
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Фев 09 2016 г., 10:28
-- Версия сервера: 5.5.41-log
-- Версия PHP: 5.4.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `top`
--

-- --------------------------------------------------------

--
-- Структура таблицы `file`
--

CREATE TABLE IF NOT EXISTS `file` (
  `id_file` int(11) NOT NULL AUTO_INCREMENT,
  `name_file` varchar(255) NOT NULL,
  `object_file` varchar(255) NOT NULL,
  `id_object_file` varchar(255) NOT NULL,
  `postfix_file` varchar(255) NOT NULL,
  PRIMARY KEY (`id_file`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=112 ;

--
-- Дамп данных таблицы `file`
--

INSERT INTO `file` (`id_file`, `name_file`, `object_file`, `id_object_file`, `postfix_file`) VALUES
(58, '/site/12/b98937b250e70700b9a4df1f87435cb1_100.jpg', 'site', '12', '_100'),
(59, '/site/13/cffc6f12baca73e8a43a424c11dcda73_100.jpg', 'site', '13', '_100'),
(60, '/site/14/56a8123ee1555004a45a69932a423f82_100.jpg', 'site', '14', '_100'),
(61, '/site/15/10deacf1ce5dbb6e8628875bf0490bbb_100.jpg', 'site', '15', '_100'),
(62, '/site/16/939fb01e48829e96465d2c50fde255d9_100.jpg', 'site', '16', '_100'),
(63, '/site/17/83964c015700bfc848c21e35d98523ae_100.jpg', 'site', '17', '_100'),
(64, '/site/18/42b451413073c88a0bca79bb38b94b81_100.jpg', 'site', '18', '_100'),
(65, '/site/19/dd5d280eb1361fb52129ae654252e051_100.jpg', 'site', '19', '_100'),
(66, '/site/20/42d2e4278b1f807f7c1d1418a01dc515_100.jpg', 'site', '20', '_100'),
(67, '/site/21/a70cc41598455259231e5e7184e8778c_100.jpg', 'site', '21', '_100'),
(68, '/site/22/f639deae27dd2b46336ac3e64c22f4da_100.jpg', 'site', '22', '_100'),
(69, '/site/23/877838cacf585e64c5bc000e1ea7ddf6_100.jpg', 'site', '23', '_100'),
(70, '/site/24/7954a3c5eb18de95873a13356b60cf5b_100.jpg', 'site', '24', '_100'),
(71, '/site/25/136ea31375bc54ee76796dd4902387d8_100.jpg', 'site', '25', '_100'),
(72, '/site/26/1ef757aaec111ce5d49255afec22c215_100.jpg', 'site', '26', '_100'),
(73, '/site/20/cf72f72c1ecec4e3ba7d7dd9c8868ee0_200.jpg', 'site', '20', '_200'),
(74, '/site/21/558f5e63bbfc4b0dd1b898ad0c787cfa_200.jpg', 'site', '21', '_200'),
(75, '/site/22/1ac139920330ded92fbe8834ab930f42_200.jpg', 'site', '22', '_200'),
(76, '/site/23/ea61ef39ca4a0364deb6b954f169945f_200.jpg', 'site', '23', '_200'),
(77, '/site/24/f2ae8baa4ba7d599e76e00a5e9107c92_200.jpg', 'site', '24', '_200'),
(79, '/site/26/62e3bfb4519ad6066687ef0777a1d7d4_200.jpg', 'site', '26', '_200'),
(80, '/site/12/c3a90c4b39273456fbd5cfa01a6899a3_210.jpg', 'site', '12', '_210'),
(81, '/site/13/f254cf0cc29f4ed4e041f5a103034ce9_210.jpg', 'site', '13', '_210'),
(82, '/site/14/8605e79cf3b544129fa48b68f51fa82b_210.jpg', 'site', '14', '_210'),
(83, '/site/15/844d82be25528ac5dbff984b2cd573ea_210.jpg', 'site', '15', '_210'),
(84, '/site/16/16fdd55198eb0f66a49a6f2468ade528_210.jpg', 'site', '16', '_210'),
(85, '/site/17/841c3d1365a08d087fedfc03a8cd831d_210.jpg', 'site', '17', '_210'),
(86, '/site/18/7d31a50ddb78209915656752a0a1094d_210.jpg', 'site', '18', '_210'),
(87, '/site/19/c98fdef5f569ba0479a224191b19b700_210.jpg', 'site', '19', '_210'),
(88, '/site/20/62c9e3eb15c7600b817a742230b3a258_210.jpg', 'site', '20', '_210'),
(89, '/site/21/cc52873f43bd5beecc156b45f3db0dce_210.jpg', 'site', '21', '_210'),
(90, '/site/22/684c4e88f005d0dcc31fa9e4f16bd8e5_210.jpg', 'site', '22', '_210'),
(91, '/site/23/a8ccd261c33bd19d5feda508c5011ed8_210.jpg', 'site', '23', '_210'),
(92, '/site/24/c3e2dca257d20727e50ed58c1d89b826_210.jpg', 'site', '24', '_210'),
(93, '/site/25/3d8bcdedd105c88ce03092b46a76da37_210.jpg', 'site', '25', '_210'),
(94, '/site/26/e1818ff0a4ed9cffc3ecd2202a4eff5f_210.jpg', 'site', '26', '_210'),
(95, '/site/25/220dbc44a3595ebabc1609b47aed8c50_200.jpg', 'site', '25', '_200'),
(96, '/site/27/e244f30e8e4af4c47380e2397cdd0e4d_100.jpg', 'site', '27', '_100'),
(97, '/site/30/119afb8212098c37c4536b7a4fe4df9b_100.jpg', 'site', '30', '_100'),
(98, '/site/31/30f422d93ed93d45a077e11fa7d7a118_100.jpg', 'site', '31', '_100'),
(99, '/site/32/bacd80533172b207b8679e87538b3aa8_100.jpg', 'site', '32', '_100'),
(100, '/site/33/55215866e9c871a93bebefe3fbd02ae1_100.jpg', 'site', '33', '_100'),
(101, '/site/27/536ba54b62b60571d237e206cc17e522_210.jpg', 'site', '27', '_210'),
(102, '/site/30/28438f4e5f2eedacbda6f461628d6bd1_210.jpg', 'site', '30', '_210'),
(103, '/site/31/e006e15cabbaee3e961912da35ac7531_210.jpg', 'site', '31', '_210'),
(104, '/site/32/3852a8be4caf59664370250452daee4d_210.jpg', 'site', '32', '_210'),
(105, '/site/33/746ca90d34dc2199623dca78d92cd954_210.jpg', 'site', '33', '_210'),
(106, '/site/34/ca3b53b3cef2e1502ab9225e7cd9e628_210.jpg', 'site', '34', '_210'),
(107, '/site/35/4052cee394f2c6683cb2608fdef44bc2_100.png', 'site', '35', '_100'),
(108, '/site/36/eeec7b04bf4adfc4004578bf987d2aff_100.png', 'site', '36', '_100'),
(109, '/site/34/dffdb0f7a86fd4e3d6366111c8a7c4a3_100.jpg', 'site', '34', '_100'),
(110, '/site/36/f37178c9aed1da23a46117e8e9026a63_210.png', 'site', '36', '_210'),
(111, '/site/35/62dd7f680ec8cad49bbc46f29520b0bb_210.png', 'site', '35', '_210');

-- --------------------------------------------------------

--
-- Структура таблицы `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id_role` int(11) NOT NULL AUTO_INCREMENT,
  `name_role` varchar(255) NOT NULL,
  PRIMARY KEY (`id_role`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `role`
--

INSERT INTO `role` (`id_role`, `name_role`) VALUES
(1, 'admin'),
(2, 'second'),
(3, 'manager');

-- --------------------------------------------------------

--
-- Структура таблицы `site`
--

CREATE TABLE IF NOT EXISTS `site` (
  `id_site` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id_site` int(11) NOT NULL,
  `name_site` varchar(255) NOT NULL,
  `url_site` text NOT NULL,
  `css_class` varchar(255) NOT NULL,
  `action` int(11) NOT NULL DEFAULT '1',
  `modal` int(1) NOT NULL DEFAULT '0',
  `rule` varchar(255) NOT NULL,
  `sortable` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_site`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=37 ;

--
-- Дамп данных таблицы `site`
--

INSERT INTO `site` (`id_site`, `parent_id_site`, `name_site`, `url_site`, `css_class`, `action`, `modal`, `rule`, `sortable`) VALUES
(12, 0, 'Веб панель продаж', 'http://192.168.0.240/webpanel/', '', 2, 0, 'site1', 5),
(13, 27, 'Сайт InfoLife', 'http://infolifes.ru/', '', 2, 0, 'site2', 6),
(14, 27, 'Английская версия InfoLife', 'http://en.infolifes.ru/', '', 2, 0, 'site3', 7),
(15, 0, 'Битрикс24', 'https://infolife.bitrix24.ru/', '', 2, 0, 'site4', 6),
(16, 0, 'Навит', 'http://navit.infolifes.ru/', '', 2, 0, 'site5', 7),
(17, 0, 'График запросов', 'http://navit.infolifes.ru/?all_graph=week''', '', 2, 0, 'site6', 8),
(18, 32, 'Отчеты из навит', 'http://navit.infolifes.ru/_libs/bitrix_report.php?type=phoner', '', 2, 1, 'site7', 11),
(19, 33, 'Заявки Навит', 'http://services.infolifes.ru/php/landst_test.php?time=today&partner=direct&type=land''', '', 2, 0, 'site8', 12),
(20, 30, 'Лэндинг Mobile', 'http://landing.infolifes.ru/il54', '', 2, 0, 'site9', 13),
(21, 30, 'Лэндинг Premium', 'http://landing.infolifes.ru/il53', '', 2, 0, 'site10', 14),
(22, 30, 'Гугловый лэндинг Mobile', 'http://landing.infolifes.ru/il61', '', 2, 0, 'site11', 15),
(23, 30, 'Гугловый лэндинг Premium', 'http://landing.infolifes.ru/il60', '', 2, 0, 'site12', 16),
(24, 30, 'Лэндинг IrisTest', 'http://iristest.infolifes.ru/iris31', '', 2, 0, 'site13', 17),
(25, 30, 'Гугловый лэндинг IrisTes', 'http://iristest.infolifes.ru/iris61', '', 2, 0, 'site14', 18),
(26, 31, 'Видео наблюдение Киров', 'http://178.208.141.164/', '', 2, 0, 'site15', 19),
(27, 0, 'Сайты', '#', '', 1, 0, 'cat1', 0),
(30, 0, 'Лендинги', '#', '', 1, 0, 'cat2', 1),
(31, 0, 'Видео наблюдение', '#', '', 1, 0, 'cat3', 2),
(32, 0, 'Отчеты', '#', '', 1, 0, 'cat4', 3),
(33, 0, 'Заявки', '#', '', 1, 0, 'cat5', 4),
(34, 31, 'Видео наблюдение Москва', 'http://178.208.141.164/', '', 2, 0, 'site16', 0),
(35, 33, 'Заявки Битрикс24', 'https://infolife.bitrix24.ru/crm/lead/?FIND=&FIND_list=t_n_ln&TITLE=&SOURCE_ID%5B%5D=&STATUS_ID%5B%5D=&OPPORTUNITY_from=&OPPORTUNITY_to=&CURRENCY_ID%5B%5D=&DATE_CREATE_datesel=today&DATE_CREATE_days=&DATE_CREATE_from=&DATE_CREATE_to=&CRM_LEAD_LIST_V12_CREATED_BY_ID_SEARCH=0&CRM_LEAD_LIST_V12_MODIFY_BY_ID_SEARCH=0&ASSIGNED_BY_ID_name=&ASSIGNED_BY_ID=&CRM_LEAD_LIST_V12_ASSIGNED_BY_ID_SEARCH=0&UF_CRM_1448973409=&grid_filter_id=&apply_filter=Y&clear_filter=&save=Y', '', 2, 0, 'site17', 0),
(36, 0, 'График отпусков', 'https://infolife.bitrix24.ru/company/absence.php', '', 2, 0, 'site18', 9);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `id_role` int(11) NOT NULL DEFAULT '3',
  `login_user` varchar(255) NOT NULL,
  `password_user` varchar(255) NOT NULL,
  `name_user` varchar(255) NOT NULL,
  `active_user` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id_user`, `id_role`, `login_user`, `password_user`, `name_user`, `active_user`) VALUES
(1, 1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Денис', '1'),
(2, 3, 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 'Денис', '1');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
