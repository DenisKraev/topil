$(document).ready(function(){

    $('.js-delete-item').click(function(){
        return confirm('Подтверждаете удаление?') ? true : false;
    });

    $('.list-items .handler').click(function(){
        $(this).parent().find('> ul').slideToggle();
        $(this).toggleClass('glyphicon-minus');
    });

//    var group = $('.sorted-table').sortable({
//        containerSelector: 'table',
//        itemPath: '> tbody',
//        itemSelector: 'tr',
//        handle: '.sort-handle',
//        placeholder: '<tr class="placeholder"/>',
//        onDrop: function ($item, container, _super) {
//            var data = group.sortable("serialize").get();
//
//            var jsonString = JSON.stringify(data, null, ' ');
//
//            console.info(jsonString);
//            _super($item, container);
//        }
//    });

    var fixHelper = function(e, ui) {
        ui.children().each(function() {
            $(this).width($(this).width());
        });
        return ui;
    };

    var sortableLinks =  $(".sorted-table tbody").sortable({
        helper: fixHelper,
        handle: ".sort-handle",
        placeholder: "placeholder",
        update: function( event, ui ) {
            var r = $(sortableLinks).sortable("toArray");
            var linkOrderData = $(sortableLinks).sortable('serialize', {
                attribute: "id"
            });

            var neworder = new Array();

            var $i=1;
            $('.sorted-table tbody tr').each(function() {
                var id  = $(this).attr("id");
                var obj = {};
                obj = id;
                neworder.push(obj);

            });

            $.ajax({
                type: "POST",
                url: '/admin.php?controller=site&action=sort&nonlayout',
                data: {data: neworder},
                dataType: 'json'
            });
        }
    }).disableSelection();


});