$(document).ready(function(){

    function datePicker(){
        $('.datepicker, .input-daterange').datepicker({
            language: 'ru'
        });
    }

    function ajaxReloadForm(){
        var url = $('.form-report').attr('action');
        $('.form-report').ajaxForm({
            url:  url+'&nonlayout',
            beforeSend: function(){
                $('#myModal').modal().find('.modal-body').html('<div class="text-center">Идет загрузка...</div>');
            },
            success: function(html){
                $('#myModal').find('.modal-body').html(html);
                datePicker();
            }
        });
    }

    $('.modal-button').click(function(e) {
        e.preventDefault();
        var url = $(this).attr('href');
        var title = $(this).data('title');
        $('#myModal .modal-title').text(title);
        $.ajax({
            url: url,
            cache: false,
            beforeSend: function(){
                $('#myModal').modal().find('.modal-body').html('<div class="text-center">Идет загрузка...</div>');
            },
            success: function(html){
                $('#myModal').find('.modal-body').html(html);
                ajaxReloadForm();
                datePicker();
            }
        });
    });

    $('#myModal').on('hidden.bs.modal', function () {
        $(this).find(".modal-body").html('');
    });

    datePicker();

    $('.download-csv').on('click', function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        window.location.assign(url);
    });

});