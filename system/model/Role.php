<?php
class Role {

    public $tableName = 'role';

    // новый для админки
    public function newAdm($data){
        $helper = new Helpers();
        $dbh = Core::db();

        $arrData = array(
            array('name_role'=>$data['name'])
        );

        try {
            $sth = $dbh->prepare("INSERT INTO role (name_role) values (:name_role)");
            foreach ($arrData as $value) {
                $sth->execute($value);
            }
            $helper->redirect('/admin/role/admin/');
        }
        catch(PDOException $e) {
            print_r($e);
        }
    }

    // редактирование для админки
    public function editAdm($data){
        $helper = new Helpers();
        $dbh = Core::db();

        $name = $data['name'];

        $arrData = array(
            array($name, $_GET['id'])
        );

        try {
            $sth = $dbh->prepare("UPDATE role SET name_role=? WHERE id_role=? ");
            foreach ($arrData as $value) {
                $sth->execute($value);
            }
            $helper->redirect('/admin/role/admin/');
            exit();
        }
        catch(PDOException $e) {
            print_r($e);
        }
    }

    public function delete($id){
        $helper = new Helpers();
        $dbh = Core::db();

        $arrData = array(
            array($id)
        );

        try {
            $sth = $dbh->prepare("DELETE FROM role WHERE id_role=?");
            foreach ($arrData as $value) {
                $sth->execute($value);
            }
            $helper->redirect('/admin/role/admin/');
        }
        catch(PDOException $e) {
            return $e;
        }
    }

    public function dataAttribute($id){
        $dbh = Core::db();
        $data = $dbh->query('SELECT * FROM '.$this->tableName.' WHERE id_role="'.$id.'"')->fetch(PDO::FETCH_ASSOC);
        return $data;
    }
}