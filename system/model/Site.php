<?php
require_once (ROOT.'/system/lib/tree/vendor/autoload.php');
require_once (ROOT.'/system/core/Core.php');
class Site {

    public $tableName = 'site';

    public function thumb($id, $postFix){
        $params = array(
            '_100' => array(
                'image_x' => 100,
                'image_ratio_y' => true,
                'image_resize' => true,
            ),
            '_210' => array(
                'image_x' => 210,
                'image_ratio_y' => true,
                'image_resize' => true,
            )
        );
        $file = new File();
        return $file->getThumb(array('id'=>$id, 'params'=>$params[$postFix], 'postFix'=> $postFix, 'table'=>$this->tableName));
    }

    // новый для админки
    public function newAdm($data){
        $helper = new Helpers();
        $file = new File();
        $dbh = Core::db();
        $config = Core::config();

        $arrData = array(
            array('name_role'=>$data['name'], 'parent_id_site'=>$data['id_cat'], 'url_site'=>$data['url_site'], 'action'=>$data['action'], 'css_class'=>$data['css_class'], 'modal'=>$data['modal'], 'rule'=>$data['rule'])
        );

        try {
            $sth = $dbh->prepare("INSERT INTO site (name_site, parent_id_site, url_site, action, css_class, modal, rule) values (:name_role, :parent_id_site, :url_site, :action, :css_class, :modal, :rule)");
            foreach ($arrData as $value) {
                $sth->execute($value);
            }
            $newId = $dbh->lastInsertId();
            $file->createImage($_FILES['image'], $config['originImageParams'], '', $newId, $this->tableName, true);
            $helper->redirect('/admin/site/admin/');
        }
        catch(PDOException $e) {
            print_r($e);
        }
    }

    // редактирование для админки
    public function editAdm($data){
        $helper = new Helpers();
        $file = new File();
        $dbh = Core::db();
        $config = Core::config();

        if($data['del_image'] && !$_FILES['image']){
            $file->delFile($_GET["id"], $this->tableName);
        }else{
            $file->createImage($_FILES['image'], $config['originImageParams'], '', $_GET['id'], $this->tableName, true);
        }

        $dataAttribute = $this->dataAttribute($_GET["id"]);
        $dataAttributeNew = $dataAttribute;
        $dataAttributeNew['name_site'] = $data['name'];
        $dataAttributeNew['url_site'] = $data['url_site'];
        $dataAttributeNew['rule'] = $data['rule'];
        $dataAttributeNew['action'] = $data['action'];
        $dataAttributeNew['css_class'] = $data['css_class'];
        $dataAttributeNew['modal'] = $data['modal'];
        if($dataAttribute['id_site'] == $data['id_cat']){
            return array('err','mess'=>array('id_cat'=>'Категория не может быть назначена сама в себя'), 'attribute'=>$dataAttribute);
        } else {
            $dataAttributeNew['parent_id_site'] = $data['id_cat'];
        }

        $arrData = array(
            array($dataAttributeNew['name_site'], $dataAttributeNew['parent_id_site'], $dataAttributeNew['url_site'], $dataAttributeNew['action'], $dataAttributeNew['css_class'], $dataAttributeNew['modal'], $dataAttributeNew['rule'], $_GET['id'])
        );

        try {
            $sth = $dbh->prepare("UPDATE site SET name_site=?, parent_id_site=?, url_site=?, action=?, css_class=?, modal=?, rule=? WHERE id_site=? ");
            foreach ($arrData as $value) {
                $sth->execute($value);
            }
            if(array_key_exists ('submitstay', $data)){
                return array('submitstay', 'attribute'=>$dataAttributeNew);
            }else{
                $id = $this->getParentId($_GET['id']);
                if($id){$redirect = '/admin/site/admin/cat/'.$id.'/';}else{$redirect = '/admin/site/admin/';}
                $helper->redirect($redirect);
            }
            exit();
        }
        catch(PDOException $e) {
            print_r($e);
        }
    }



    public function getAncestors($id){
        $dbh = Core::db();
        $records = $dbh->query('SELECT *, (id_site) as id, (parent_id_site) as parent FROM site ORDER BY sortable ASC')->fetchAll(PDO::FETCH_ASSOC);
        $data = new BlueM\Tree($records);
        $node = $data->getNodeById($id);
        $ancestors = $node->getAncestorsAndSelf();
        return $this->getTreeAncestors($ancestors);
    }

    public function getTreeSites(){
        $dbh = Core::db();
        $records = $dbh->query('SELECT *, (id_site) as id, (parent_id_site) as parent FROM site ORDER BY sortable ASC')->fetchAll(PDO::FETCH_ASSOC);
        $data = new BlueM\Tree($records);
        $rootNodes = $data->getRootNodes();
        return $this->getTree($rootNodes, $data);
    }

    public function getParentId($id){
        $dbh = Core::db();
        $records = $dbh->query('SELECT *, (id_site) as id, (parent_id_site) as parent FROM site ORDER BY sortable ASC')->fetchAll(PDO::FETCH_ASSOC);
        $data = new BlueM\Tree($records);
        $node = $data->getNodeById($id);
        $parent = $node->getParent();
        return $parent->id;
    }

    public function getChildrenSites($id){
        $dbh = Core::db();
        $records = $dbh->query('SELECT *, (id_site) as id, (parent_id_site) as parent FROM site ORDER BY sortable ASC')->fetchAll(PDO::FETCH_ASSOC);
        $data = new BlueM\Tree($records);
        $nodes = $data->getNodeById($id);
        $arr = array();
        foreach($nodes->getChildren() as $i=>$item){
            $arr[$i]['name'] = $item->name_site;
            $arr[$i]['url'] = $item->url_site;
            $arr[$i]['id'] = $item->id_site;
            $arr[$i]['rule'] = $item->rule;
            $arr[$i]['action'] = $item->action;
            $arr[$i]['css_class'] = $item->css_class;
            $arr[$i]['sortable'] = $item->sortable;
            $arr[$i]['modal'] = $item->modal;
            $arr[$i]['count'] = count($item->children)>0?count($item->children):'';
        }
        return $arr;
    }

    public function getRootSites(){
        $dbh = Core::db();
        $records = $dbh->query('SELECT *, (id_site) as id, (parent_id_site) as parent FROM site ORDER BY sortable ASC')->fetchAll(PDO::FETCH_ASSOC);
        $data = new BlueM\Tree($records);
        $arr = array();
        foreach($data->getRootNodes() as $i=>$item){
            $arr[$i]['name'] = $item->name_site;
            $arr[$i]['url'] = $item->url_site;
            $arr[$i]['id'] = $item->id_site;
            $arr[$i]['rule'] = $item->rule;
            $arr[$i]['action'] = $item->action;
            $arr[$i]['css_class'] = $item->css_class;
            $arr[$i]['sortable'] = $item->sortable;
            $arr[$i]['modal'] = $item->modal;
            $arr[$i]['count'] = count($item->children)>0?count($item->children):'';
        }
        return $arr;
    }

    public function getTree($nodes, $data){
        $arr = array();
        foreach($nodes as $i=>$item){
            $arr[$i]['name'] = $item->name_site;
            $arr[$i]['url'] = $item->url_site;
            $arr[$i]['id'] = $item->id_site;
            $arr[$i]['rule'] = $item->rule;
            $arr[$i]['action'] = $item->action;
            $arr[$i]['css_class'] = $item->css_class;
            $arr[$i]['modal'] = $item->modal;
            $arr[$i]['sortable'] = $item->sortable;
            $arr[$i]['count'] = count($item->children)>0?count($item->children):'';
            if($item->children){
                $node = $data->getNodeById($item->id);
                $children = $node->getChildren();
                $arrChildren = $this->getTree($children, $data);
                $arr[$i]['children'] = $arrChildren;
            }
        }
        return $arr;
    }

    public function getTreeAncestors($nodes){
        $arr = array();
        foreach($nodes as $i=>$item){
            if($item->parent == ''){continue;}
            $arr[$i]['name'] = $item->name_site;
            $arr[$i]['id'] = $item->id_site;
        }
        return $arr;
    }

    public function delete($id){
        $helper = new Helpers();
        $file = new File();
        $dbh = Core::db();

        $file->delFile($id, $this->tableName);

        $arrData = array(
            array($id)
        );

        try {
            $sth = $dbh->prepare("DELETE FROM site WHERE id_site=?");
            foreach ($arrData as $value) {
                $sth->execute($value);
            }
            $helper->redirect('/admin/site/admin/');
        }
        catch(PDOException $e) {
            return $e;
        }
    }

    public function dataAttribute($id){
        $dbh = Core::db();
        $data = $dbh->query('SELECT * FROM '.$this->tableName.' WHERE id_site="'.$id.'"')->fetch(PDO::FETCH_ASSOC);
        return $data;
    }
}