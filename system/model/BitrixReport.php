<?php
class BitrixReport {

    public $url = 'https://infolife.bitrix24.ru/rest/';
    public $token;
    public $bitrixusers;

    function __construct() {
        $this->token = file_get_contents('http://navit.infolifes.ru/_libs/rest.php?GetToken');
        $this->bitrixusers = ROOT.'/data/bitrix/users.bit';
    }

    public function getUsers(){
        $request = array();

        $resp=$this->http('department.get',$request);
        $_DEPARTMENT[]=31;
        foreach($resp['result'] as $k=>&$v)
            if($v['PARENT']==$_DEPARTMENT[0])
                $_DEPARTMENT[]=$v['ID'];

        $request['ACTIVE']=1;
        foreach($_DEPARTMENT as $k=>&$v){
            $request['UF_DEPARTMENT']=$v;
            $resp=$this->http('user.get', $request);
            foreach($resp['result'] as $ke=>&$va)
                if($va['ID']!=87 && $va['ID']!=93 && $va['ID']!=37)
                    $users[$va['ID']]=$va;
        }
        //$this->save($this->bitrixusers, json_encode($users));

        return $users;
    }

    public function http($a,$request){
        $request['auth'] = $this->token;
        $resp=array();
        $b=true;$i=0;
        while($b){
            $response=json_decode(file_get_contents($this->url.$a.'.json?'.http_build_query($request)), true);
            if($response['next'])
                $request['start']=$response['next'];
            else
                $b=false;
            $resp = array_merge($resp, $response['result']);
        }
        return array('result'=>$resp);
    }

    public function save($file, $content){
        $f = fopen($file, 'w+');
        fwrite($f, $content);
        fclose($f);
        chmod($file, 0777);
    }

    public function report($focus){
       $create = 'create'.$focus;
       $report = $this->$create();
       return $report;
    }

    public function createMinimarket(){
        $dbh2 = Core::db('dbh2');

        $date_s = (($_REQUEST['date_start']?$_REQUEST['date_start']:date('Y:m:d')).' 00:00:00');
        $date_e = (($_REQUEST['date_end']?$_REQUEST['date_end']:date('Y:m:d',strtotime($date_s))).' 23:59:59');

        $date_s_unix = strtotime($date_s)+14400;
        $date_e_unix = strtotime($date_e)+14400;

        $dataArr = array(
            array('table_postfix'=>'stat', 'area'=>'yandex_il', 'destination'=>'count'),
            array('table_postfix'=>'land', 'area'=>'yandex_il', 'destination'=>'lead'),
            array('table_postfix'=>'stat_adw', 'area'=>'google_il', 'destination'=>'count'),
            array('table_postfix'=>'land_adw', 'area'=>'google_il', 'destination'=>'lead'),
            array('table_postfix'=>'stat_iris', 'area'=>'yandex_it', 'destination'=>'count'),
            array('table_postfix'=>'land_iris', 'area'=>'yandex_it', 'destination'=>'lead'),
            array('table_postfix'=>'stat_iris_adw', 'area'=>'google_it', 'destination'=>'count'),
            array('table_postfix'=>'land_iris_adw', 'area'=>'google_it', 'destination'=>'lead'),
        );

        $out = array();
        foreach($dataArr as $item) {
            $sql = 'SELECT `date`, COUNT(DATE_FORMAT(FROM_UNIXTIME(`date`), "%d%m")) AS avg FROM infinity__'.$item['table_postfix'].' WHERE date > '.$date_s_unix.' AND date < '.$date_e_unix.' GROUP BY DATE_FORMAT(FROM_UNIXTIME(`date`) , "%d%m") ORDER BY  `date` ASC';
            $records = $dbh2->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            foreach($records as $recordItem){
                $out[$item['area'].'_'.$item['table_postfix']][date('d-m-Y',$recordItem['date'])][$item['destination']]=$recordItem['avg'];
            }
        }

        return $out;
    }

    public function createDaily(){
        $users = $this->getUsers();

        $date_s = (($_REQUEST['date_start']?$_REQUEST['date_start']:date('Y:m:d')).' 00:00:00');
        $date_e = (($_REQUEST['date_end']?$_REQUEST['date_end']:date('Y:m:d',strtotime($date_s))).' 23:59:59');

        $date_s_p = date('d.m.Y G:i:s', strtotime($date_s));
        $date_to_p = date('d.m.Y G:i:s', strtotime($date_e));

        $request['select[0]']='ID';
        $request['select[1]']='ASSIGNED_BY_ID';
        $request['select[2]']='STATUS_ID';
        $request['filter[>DATE_MODIFY]']=$date_s;
        $request['filter[<DATE_MODIFY]']=$date_e;
        $resp=$this->http('crm.lead.list',$request);
        foreach($resp['result'] as $k=>&$v){
            $users[$v['ASSIGNED_BY_ID']]['LEAD_NUM']++;
            if($v['STATUS_ID']==6||$v['STATUS_ID']==7){
                $users[$v['ASSIGNED_BY_ID']]['COMMUNICATIONS'][4]++;
            }
        }

        unset($request);
        $request['select[0]']='ID';
        $request['select[1]']='AUTHOR_ID';
        //$request['select[2]']='COMMUNICATIONS';
        $request['select[2]']='TYPE_ID';
        $request['filter[>DEADLINE]']=$date_s;
        $request['filter[<DEADLINE]']=$date_e;
        $request['filter[COMPLETED]']='Y';
        $resp=$this->http('crm.activity.list',$request);
        foreach($resp['result'] as $k=>&$v){
            if($v["TYPE_ID"]!=4){
                $users[$v['AUTHOR_ID']]['COMMUNICATIONS'][$v["TYPE_ID"]]++;
            }
        }

        unset($request);
        $request['select[0]']='ID';
        $request['select[1]']='RESPONSIBLE_ID';
        $request['select[2]']='PRICE';
        //$request['select[2]']='PAYED';
        $request['filter[>DATE_STATUS]']=$date_s;
        $request['filter[<DATE_STATUS]']=$date_e;
        $request['filter[!STATUS_ID]']='P';
        $resp=$this->http('crm.invoice.list',$request);
        //echo '<pre>';var_dump($resp);echo '</pre>';
        foreach($resp['result'] as $k=>&$v){
            $users[$v['RESPONSIBLE_ID']]['BILL_COUNT']++;
            $users[$v['RESPONSIBLE_ID']]['BILL_SUMM']+=$v['PRICE'];
        }


        unset($request);
        $request['select[0]']='ID';
        $request['select[1]']='RESPONSIBLE_ID';
        $request['select[2]']='PRICE';
        //$request['select[2]']='PAYED';
        $request['filter[>DATE_PAYED]']=$date_s;
        $request['filter[<DATE_PAYED]']=$date_e;
        $resp=$this->http('crm.invoice.list',$request);
        foreach($resp['result'] as $k=>&$v){
            $users[$v['RESPONSIBLE_ID']]['BILL_PAYED_SUMM']+=$v['PRICE'];
        }

        return $users;
    }

    public function createLeadstime(){
        $users = $this->getUsers();

        $date_s = (($_REQUEST['date_start']?$_REQUEST['date_start']:date('Y:m:d')).' 00:00:00');
        $date_e = (($_REQUEST['date_end']?$_REQUEST['date_end']:date('Y:m:d',strtotime($date_s))).' 23:59:59');

        $date_s_p = date('d.m.Y G:i:s', strtotime($date_s));
        $date_to_p = date('d.m.Y G:i:s', strtotime($date_e));

        $request['select[0]']='ID';
        $request['select[1]']='ASSIGNED_BY_ID';
        $request['select[2]']='UF_CRM_1447334240';
        $request['select[3]']='UF_CRM_1450248450';
        $request['select[4]']='DATE_CREATE';
        $request['filter[>DATE_CREATE]'] = $date_s_p;
        $request['filter[<DATE_CREATE]'] = $date_to_p;
        $request['filter[>UF_CRM_1450248450]'] = '01.01.1970 3:00:00';
        $resp = $this->http('crm.lead.list', $request);

        $res = array();
        foreach($resp['result'] as $k=>&$v){
            $users[$v['ASSIGNED_BY_ID']]['DATE_CREATE']['UF_CRM_1447334240']['UF_CRM_1450248450'];
            $res_tome = (strtotime($v['UF_CRM_1447334240']))-(strtotime($v['UF_CRM_1450248450']));
            $month = floor($res_tome / 2592000);
            $day = ($res_tome / 86400) % 30;
            $hour = ($res_tome / 3600) % 24;
            $min = ($res_tome / 60) % 60;
            $sec = $res_tome % 60;
            $res[] = array(
                'name'=>$users[$v['ASSIGNED_BY_ID']]['LAST_NAME'].' '.$users[$v['ASSIGNED_BY_ID']]['NAME'],
                'phone'=>$v['PHONE_NUMBER'],
                'date_create'=>date('d.m.Y G:i:s',strtotime($v['DATE_CREATE'])),
                'date_distribution'=>date('d.m.Y G:i:s',strtotime($v['UF_CRM_1450248450'])),
                'date_change'=>date('d.m.Y G:i:s',strtotime($v['UF_CRM_1447334240'])),
                'reaction_time'=>$day.'дн.'.$hour.'час.'.$min.'мин.'.$sec.'сек',
                'id_lead'=>$v['ID']
            );
        }

        return $res;
    }

    public function createMissedcalls(){
        $users = $this->getUsers();

        $date_s = (($_REQUEST['date_start']?$_REQUEST['date_start']:date('Y:m:d')).' 00:00:00');
        $date_e = (($_REQUEST['date_end']?$_REQUEST['date_end']:date('Y:m:d',strtotime($date_s))).' 23:59:59');

        $date_s_p = date('d.m.Y G:i:s', strtotime($date_s));
        $date_to_p = date('d.m.Y G:i:s', strtotime($date_e));

        $request['select[0]']='ID';
        $request['select[1]']='PORTAL_USER_ID';
        $request['select[2]']='PHONE_NUMBER';
        $request['filter[>CALL_START_DATE]'] = $date_s_p;
        $request['filter[<CALL_START_DATE]'] = $date_to_p;
        $request['filter[<CALL_FAILED_CODE]'] = 304;
        $request['filter[CALL_TYPE]'] = 2;
        $resp = $this->http('voximplant.statistic.get', $request);

        $res = array();
        foreach($resp['result'] as $k=>&$v){
            $users[$v['PORTAL_USER_ID']]['PHONE_NUMBER']['CALL_TYPE']['CALL_START_DATE'];
            $res[] = array(
                'name'=>$users[$v['PORTAL_USER_ID']]['LAST_NAME'].' '.$users[$v['PORTAL_USER_ID']]['NAME'],
                'phone'=>$v['PHONE_NUMBER'],
                'date'=>date('d.m.Y',strtotime($v['CALL_START_DATE'])),
                'time'=>date('G:i:s',strtotime($v['CALL_START_DATE']))
            );
        }

        return $res;
    }

    public function createPhoner(){
        $users = $this->getUsers();

        $date_s = (($_REQUEST['date_start']?$_REQUEST['date_start']:date('Y:m:d')).' 00:00:00');
        $date_e = (($_REQUEST['date_end']?$_REQUEST['date_end']:date('Y:m:d',strtotime($date_s))).' 23:59:59');

        $date_s_p = date('d.m.Y G:i:s', strtotime($date_s));
        $date_to_p = date('d.m.Y G:i:s', strtotime($date_e));

        $request['select[0]']='ID';
        $request['select[1]']='ASSIGNED_BY_ID';
        $request['select[2]']='STATUS_ID';
        $request['filter[>DATE_CREATE]']=$date_s;
        $request['filter[<DATE_CREATE]']=$date_e;
        $resp = $this->http('crm.lead.list',$request);
        foreach($resp['result'] as $k=>&$v){
            $users[$v['ASSIGNED_BY_ID']]['LEAD_CR_NUM']++;
        }

        unset($request);
        $request['select[0]']='ID';
        $request['select[1]']='ASSIGNED_BY_ID';
        $request['select[2]']='STATUS_ID';
        $request['filter[>DATE_MODIFY]']=$date_s;
        $request['filter[<DATE_MODIFY]']=$date_e;
        $resp = $this->http('crm.lead.list',$request);
        foreach($resp['result'] as $k=>&$v){
            $users[$v['ASSIGNED_BY_ID']]['LEAD_NUM']++;
        }

        unset($request);
        $request['select[0]']='ID';
        $request['select[1]']='AUTHOR_ID';
        //$request['select[2]']='CALL_DURATION';
        $request['filter[>CREATED]']=$date_s_p;
        $request['filter[<CREATED]']=$date_to_p;
        $request['filter[TYPE_ID]']=4;
        $resp = $this->http('crm.activity.list',$request);
        foreach($resp['result'] as $k=>&$v){
            $users[$v['AUTHOR_ID']]['EMAIL_COUNT']++;
        }

        unset($request);
        $request['select[0]']='ID';
        $request['select[1]']='PORTAL_USER_ID';
        $request['select[2]']='CALL_DURATION';
        $request['filter[>CALL_START_DATE]']=$date_s_p;
        $request['filter[<CALL_START_DATE]']=$date_to_p;
        $request['filter[CALL_FAILED_CODE]']=200;
        $request['filter[>CALL_DURATION]']=59;
        $resp = $this->http('voximplant.statistic.get',$request);
        foreach($resp['result'] as $k=>&$v){
            $users[$v['PORTAL_USER_ID']]['CALL_COUNT1']++;
            $users[$v['PORTAL_USER_ID']]['CALL_SUMM1']+=$v['CALL_DURATION'];
        }

        unset($request);
        $request['select[0]']='ID';
        $request['select[1]']='PORTAL_USER_ID';
        $request['select[2]']='CALL_DURATION';
        $request['filter[>CALL_START_DATE]']=$date_s_p;
        $request['filter[<CALL_START_DATE]']=$date_to_p;
        $request['filter[CALL_FAILED_CODE]']=200;
        $request['filter[>CALL_DURATION]']=119;
        $resp = $this->http('voximplant.statistic.get',$request);
        foreach($resp['result'] as $k=>&$v){
            $users[$v['PORTAL_USER_ID']]['CALL_COUNT']++;
            $users[$v['PORTAL_USER_ID']]['CALL_SUMM']+=$v['CALL_DURATION'];
        }

        unset($request);
        $request['select[0]']='ID';
        $request['select[1]']='PORTAL_USER_ID';
        $request['select[2]']='CALL_DURATION';
        $request['filter[>CALL_START_DATE]']=$date_s_p;
        $request['filter[<CALL_START_DATE]']=$date_to_p;
        $request['filter[CALL_FAILED_CODE]']=200;
        //$request['filter[>CALL_DURATION]']=400;
        $resp = $this->http('voximplant.statistic.get',$request);
        foreach($resp['result'] as $k=>&$v){
            $users[$v['PORTAL_USER_ID']]['CALL_ALL_COUNT']++;
            $users[$v['PORTAL_USER_ID']]['CALL_ALL_SUMM']+=$v['CALL_DURATION'];
        }

        unset($request);
        $request['select[0]']='ID';
        $request['select[1]']='PORTAL_USER_ID';
        $request['filter[>CALL_START_DATE]']=$date_s_p;
        $request['filter[<CALL_START_DATE]']=$date_to_p;
        $request['filter[<CALL_FAILED_CODE]']=304;
        $request['filter[CALL_TYPE]']=2;
        $resp = $this->http('voximplant.statistic.get',$request);
        foreach($resp['result'] as $k=>&$v){
            $users[$v['PORTAL_USER_ID']]['MISS_COUNT']++;
        }

        return $users;
    }
}