<?php
require_once (ROOT.'/system/lib/upload/class.upload.php');
class File {

    public $tableName = 'file';

    // Создание картинки и занесение в о ней сведений базу даных,
    // если параметр $origin = true, то создается оригинальный файл в директорию origin (по умолчанию), из которого будут создаваться миниатюры,
    // в таком случае запись в базе данных не создается.
    // Параметры оригинального файла хранятся в файле конфигурации
    public function createImage($fileData, $params, $postFix, $subDir, $dir, $origin = false){
        $dbh = Core::db();
        $handle = new upload($fileData);

        if($origin){
            $folder = ROOT.'/content/'.$dir.'/'.$subDir.'/origin/';
        }else{
            $folder = ROOT.'/content/'.$dir.'/'.$subDir.'/';
        }

        if($handle->uploaded) {

            $newFileName = is_array($fileData)?$fileData['name']:'';
            $handle->file_name_body_add = $postFix;
            $handle->file_new_name_body = md5(microtime().$newFileName);
            foreach($params as $param => $value){
                $handle->{$param} = $value;
            }
            $handle->process($folder);
            if ($handle->processed) {
                if(!$origin){
                    $arrData = array(
                        array('name_file'=>'/'.$dir.'/'.$subDir.'/'.$handle->file_dst_name, 'object_file'=>$dir, 'id_object_file'=>$subDir, 'postfix_file'=>$postFix)
                    );

                    try {
                        $sth = $dbh->prepare("INSERT INTO file (name_file, object_file, id_object_file, postfix_file) values (:name_file, :object_file, :id_object_file, :postfix_file)");
                        foreach ($arrData as $value) {
                            $sth->execute($value);

                        }
                    }

                    catch(PDOException $e) {
                        print_r($e);
                    }
                    return $arrData[0]['name_file'];
                }
            } else {
                print_r($handle->error);
            }
        }
    }

    // Возвращает имя новой или кешированной миниатюрки
    public function getThumb( $params){
        $dbh = Core::db();

        $folderOrigin = ROOT.'/content/'.$params['table'].'/'.$params['id'].'/origin';

        $files = $this->getFiles($folderOrigin);
        $fileOrigin = '';
        if(is_array($files)){
            $fileOrigin = $files[0];
        }
        $fileData = $folderOrigin.'/'.$fileOrigin;

        if($fileOrigin != ''){
            $path = $dbh->query('SELECT id_file, name_file FROM file WHERE id_object_file = "'.$params['id'].'" AND postfix_file = "'.$params['postFix'].'" AND object_file = "'.$params['table'].'"')->fetch(PDO::FETCH_ASSOC);

            if($path['name_file'] && file_exists(ROOT.'/content'.$path['name_file'])){
                return $path['name_file'];
            } else {
                $this->delete($path['id_file']);
                return $this->createImage($fileData, $params['params'], $params['postFix'], $params['id'], $params['table']);
            }
        }else{
            return false;
        }
    }

    // Удаление файла, удаление данных из базы данных, удаление всех миниатюрок, для картинки удаление оригинала
    public function delFile($itemId, $ojName, $image = true){
        $files = $this->getItemFile($itemId);
        foreach($files as $file){
            $fileIds[] = $file['id_file'];
            $fileName = ROOT.'/content'.$file['name_file'];
            unlink($fileName);
        }
        if($image){
            $filePathOrigin = ROOT.'/content/'.$ojName.'/'.$itemId.'/origin/';
            $this->removeDirectory($filePathOrigin);
        }
        $this->delete($fileIds);
    }

    // Получение всех файлов принадлежащих указанной директории
    public function getFiles($fileData){
        $arr = array();
        if(is_dir($fileData)){
            if ($handle = opendir($fileData)) {
                while (false !== ($file = readdir($handle))) {
                    if($file == '.'||$file == '..'){continue;}
                    $arr[] = $file;

                }
                closedir($handle);
            }
        }
        return $arr;
    }

    // Удаление указанной директории и всех принадлежащих к ней файлов
    public function removeDirectory($dir) {
        if ($objs = glob($dir."/*")) {
            foreach($objs as $obj) {
                is_dir($obj) ? $this->removeDirectory($obj) : unlink($obj);
            }
        }
        if(is_dir($dir)){
            rmdir($dir);
        }
    }

    // Удаление данных о файле или группе файлов из базы данных
    public function delete($delIds = array()){
        $dbh = Core::db();

        if(!is_array($delIds)){
            $delIds = array($delIds);
        }

        foreach($delIds as $item){
            $ids[] = (int)$item;
        }

        try {
            $sth = $dbh->prepare("DELETE FROM file WHERE id_file in (".implode(',', $ids).")");
            $sth->execute();
        }
        catch(PDOException $e) {
            print_r($e);
        }
    }

    // Получение всех записей о файле по идентификатору объекта к которому он относится
    public function getItemFile($id){
        $dbh = Core::db();
        $data = $dbh->query('SELECT id_file, name_file FROM '.$this->tableName.' WHERE id_object_file="'.$id.'"')->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }

    // Возвращает данные файла из базы по указанному идентификатору
    public function dataAttribute($id){
        $dbh = Core::db();
        $data = $dbh->query('SELECT * FROM '.$this->tableName.' WHERE id_file="'.$id.'"')->fetch(PDO::FETCH_ASSOC);
        return $data;
    }
}