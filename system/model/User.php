<?php
class User {

    public $user_id = '';
    public $tableName = 'user';

    function __construct(){
        $this->user_id = isset($_COOKIE['user_id'])?$_COOKIE['user_id']:'';
    }

    public function isGuest(){
        if($this->user_id == '' || $this->user_id == null){
            return true;
        } else {
            return false;
        }
    }

    public function getUser(){
        $dbh = Core::db();
        $helper = new Helpers();
        if(!$this->isGuest()){
            $user = $dbh->query('SELECT * FROM user LEFT JOIN role ON user.id_role = role.id_role WHERE id_user = '.$this->user_id)->fetch(PDO::FETCH_ASSOC);
            if(!$user['active_user']){
                $helper->redirect('/user/logout/');
            } else {
                return $user;
            }
        } else {
            return false;
        }
    }

    public function access($controller, $rule){
        $helper = new Helpers();
        $user = $this->getUser();
        if(!$user){
            $helper->redirect('/user/login/');
        } else {
            $role = new Rule();
            $mySetRoles = array();
            $mySetRoles = $role->setRules[$user['name_role']][$controller];
            if(!empty($mySetRoles)){
                if(!in_array($rule, $mySetRoles)){
                    return false;
                }else{
                    return true;
                }
            }else{
                return false;
            }
        }
    }

    // авторизация
    public function auth($login, $password){
        $helper = new Helpers();
        $data = $this->validAuth($login,$password);
        if(is_array($data) && in_array('err',$data)){
            return $data;
        } else {
            setcookie("user_id", $data, time() + 604800, '/');
            $helper->log('system', 'act: login | user_id: '.$data);
            $helper->redirect('/');
        }
    }

    // валидация авторизации
    private function validAuth($login,$password){
        if($this->rule_is_empty($login,$password)){
            return array('err','mess'=>array('all'=>'Необходимо заполнить все поля'));
        }
        $user = $this->rule_is_isset('login_user', $login);
        if(!$user){
            return array('err','mess'=>array('login'=>'Пользователь с таким логином не найден'));
        }
        if(!$user['active_user']){
            return array('err','mess'=>array('all'=>'Пользователь не активирован'));
        }
        if($this->rule_conform_password($user['password_user'], $password)){
            return $user['id_user'];
        } else {
            return array('err','mess'=>array('password'=>'Неверный пароль'));
        }
    }

    // новый для админки
    public function newAdm($data){
        $helper = new Helpers();
        $dbh = Core::db();

        $user = $this->rule_is_isset('login_user', $data['login']);
        if($user){
            return array('err','mess'=>array('login'=>'Пользователь с таким логином уже существует'));
        }

        $arrData = array(
            array('id_role'=>$data['id_role'], 'name_user'=>$data['name'], 'login_user'=>$data['login'], 'password_user'=>md5($data['password']), 'active_user'=>$data['active'])
        );

        try {
            $sth = $dbh->prepare("INSERT INTO user (id_role, name_user, login_user, password_user, active_user) values (:id_role, :name_user, :login_user, :password_user, :active_user)");
            foreach ($arrData as $value) {
                $sth->execute($value);
            }
            $helper->redirect('/admin/user/admin/');
        }
        catch(PDOException $e) {
            print_r($e);
        }
    }

    // редактирование для админки
    public function editAdm($data){
        $helper = new Helpers();
        $dbh = Core::db();

        $dataAttribute = $this->dataAttribute($_GET["id"]);
        $dataAttributeNew = $dataAttribute;
        $dataAttributeNew['id_role'] = $data['id_role'];
        $dataAttributeNew['name_user'] = $data['name'];
        if($dataAttribute['login_user'] != $data['login']){
            $user = $this->rule_is_isset('login_user', $data['login'], $dbh);
            if($user){
                return array('err','mess'=>array('login'=>'Пользователь с таким логином уже существует'), 'attribute'=>$dataAttribute);
            }
        } else {
            $dataAttributeNew['login_user'] = $data['login'];
        }
        if($data['password'] != ''){
            $dataAttributeNew['password_user'] = md5($data['password']);
        }
        $dataAttributeNew['active_user'] = $data['active'];

        $arrData = array(
            array($dataAttributeNew['id_role'], $dataAttributeNew['name_user'], $dataAttributeNew['login_user'], $dataAttributeNew['password_user'], $dataAttributeNew['active_user'], $_GET['id'])
        );

        try {
            $sth = $dbh->prepare("UPDATE user SET id_role=?, name_user=?, login_user=?, password_user=?, active_user=? WHERE id_user=?");
            foreach ($arrData as $value) {
                $sth->execute($value);
            }
            $helper->redirect('/admin/user/admin/');
            exit();
        }
        catch(PDOException $e) {
            print_r($e);
        }
    }

    // регистрация
    public function reg($data){
        $helper = new Helpers();
        $validReg = $this->validReg($data);
        if(is_array($validReg) && in_array('err',$validReg)){
            return $validReg;
        } else {
            $newUser = $this->newUser($data);
            if(is_array($newUser) && in_array('err',$newUser)){
                return $newUser;
            }else{
                $helper->redirect('/user/login/');
            }
        }
    }

    // валидация регистрации
    private function validReg($data){
        if($this->rule_is_empty_reg($data)){
            return array('err','mess'=>array('all'=>'Необходимо заполнить все поля'));
        }
        $user = $this->rule_is_isset('login_user', $data['login']);
        if($user){
            return array('err','mess'=>array('login'=>'Пользователь с таким логином уже существует'));
        }
        if($data['code'] != $data['captcha_verify']){
            return array('err','mess'=>array('code'=>'Проверочный код не совпадает'));
        }
        return true;
    }

    // создание пользователя
    private function newUser($data){
        $dbh = Core::db();
        $arrData = array(
            array("login_user" => $data['login'], "password_user" => md5($data['password']), 'name_user' => $data['name'])
        );

        try {
            $sth = $dbh->prepare("INSERT INTO user (login_user, password_user, name_user) values (:login_user, :password_user, :name_user)");
            foreach ($arrData as $value) {
                $sth->execute($value);
            }
            return true;
        }
        catch(PDOException $e) {
            return array('err','mess'=>array('all'=>$e));
        }
    }

    // правила валидации
    private function rule_is_empty_reg($data){
        $res = false;
        foreach($data as $item){
            if(empty($item)){
                $res = true;
            }
        }
        return $res;
    }

    private function rule_is_empty($login, $password){
        if(empty($login)||empty($password)){
            return true;
        }else{
            return false;
        }
    }

    private function rule_is_isset($field, $value){
        $dbh = Core::db();
        $data = $dbh->query('SELECT * FROM user WHERE '.$field.' = "'.$value.'"')->fetch(PDO::FETCH_ASSOC);
        if(count($data)>0){
            return $data;
        } else {
            return false;
        }
    }

    private function rule_conform_password($password, $password_post){
        if($password == md5($password_post)){
            return true;
        } else {
            return false;
        }
    }

    public function delete($id){
        $helper = new Helpers();
        $dbh = Core::db();

        $arrData = array(
            array($id)
        );

        try {
            $sth = $dbh->prepare("DELETE FROM user WHERE id_user=?");
            foreach ($arrData as $value) {
                $sth->execute($value);
            }
            return $helper->redirect('/admin/user/admin/');
        }
        catch(PDOException $e) {
            return $e;
        }
    }

    public function dataAttribute($id){
        $dbh = Core::db();
        $data = $dbh->query('SELECT * FROM '.$this->tableName.' WHERE id_user="'.$id.'"')->fetch(PDO::FETCH_ASSOC);
        return $data;
    }
}