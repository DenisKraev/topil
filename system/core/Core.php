<?php
require_once (ROOT.'/system/connect.php');
require_once (ROOT.'/system/helpers/Helpers.php');
require_once (ROOT.'/system/rule/Rule.php');
require_once (ROOT.'/system/model/User.php');
require_once (ROOT.'/system/model/File.php');
class Core {

    public static $layout;

    public static function config(){
        global $config;
        return $config;
    }

    public static function user(){
        $user = new User();
        return $user;
    }

    public static function rule(){
        $user = new Rule();
        return $user;
    }

    public static function db($name = 'dbh'){
        global $$name;
        return $$name;
    }

    public static function controller(){
        $config = self::config();
        if(ADMIN){$dc = $config['defaultAdminController'];}
        if(MAIN){$dc = $config['defaultController'];}
        $c = (!isset($_GET['controller']))?$dc:$_GET['controller'];
        return ucfirst($c);
    }

    public static function action(){
        $c = self::controller().'Controller';
        $c = new $c();
        $da = $c->defaultAction;
        $da = (!isset($da))?'Index':$da;
        $a = (!isset($_GET['action']))?$da:$_GET['action'];
        return ucfirst($a);
    }

    public static function layout(){
       $l = self::$layout;
        if($l == ''){
            if(ADMIN){
                return 'admin';
            }
            if(MAIN){
                return 'main';
            }
        }else{
            return $l;
        }
    }

    public static function create(){
        $user = self::user();

        $nController = self::controller().'Controller';
        $controller = new $nController();
        $action = 'action'.self::action();
        $data = $controller->{$action}();

        if(!isset($_GET['nonlayout'])) {
            $helper = new Helpers();
            return $helper->render(ROOT."/system/view/layout/".self::layout().".php", array('data'=>$data, 'user'=>$user));
        } else {
            return $data;
        }
    }

}