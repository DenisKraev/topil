<?php
class Rule {
    public $setRules = array(
        'admin' => array(
            'user'=>array('admin','view','edit','create','delete'),
            'role'=>array('admin','view','edit','create','delete'),
            'site'=>array('admin','view','edit','create','delete'),
            'bitrixreport'=>array('admin','view','edit','create','delete'),

            'cat1'=>array('admin','view','edit','create','delete'),
            'cat2'=>array('admin','view','edit','create','delete'),
            'cat3'=>array('admin','view','edit','create','delete'),
            'cat4'=>array('admin','view','edit','create','delete'),
            'cat5'=>array('admin','view','edit','create','delete'),
            'site1'=>array('admin','view','edit','create','delete'),
            'site2'=>array('admin','view','edit','create','delete'),
            'site3'=>array('admin','view','edit','create','delete'),
            'site4'=>array('admin','view','edit','create','delete'),
            'site5'=>array('admin','view','edit','create','delete'),
            'site6'=>array('admin','view','edit','create','delete'),
            'site7'=>array('admin','view','edit','create','delete'),
            'site8'=>array('admin','view','edit','create','delete'),
            'site9'=>array('admin','view','edit','create','delete'),
            'site10'=>array('admin','view','edit','create','delete'),
            'site11'=>array('admin','view','edit','create','delete'),
            'site12'=>array('admin','view','edit','create','delete'),
            'site13'=>array('admin','view','edit','create','delete'),
            'site14'=>array('admin','view','edit','create','delete'),
            'site15'=>array('admin','view','edit','create','delete'),
            'site16'=>array('admin','view','edit','create','delete'),
            'site17'=>array('admin','view','edit','create','delete'),
            'site18'=>array('admin','view','edit','create','delete'),
            'site19'=>array('admin','view','edit','create','delete'),
            'site20'=>array('admin','view','edit','create','delete'),
            'site21'=>array('admin','view','edit','create','delete'),
            'site22'=>array('admin','view','edit','create','delete'),
        ),
        'manager' => array(
            //'bitrixreport'=>array('view'),
            'cat1'=>array('view'),//
            'cat2'=>array('view'),//
            'site2'=>array('view'),//
            'site3'=>array('view'),//
            'site4'=>array('view'),//
            'site5'=>array('view'), //
            'site9'=>array('view'),//
            'site10'=>array('view'),//
            'site11'=>array('view'),//
            'site12'=>array('view'),//
            'site13'=>array('view'),//
            'site14'=>array('view'),//
        ),
        'manager1' => array(
            //'bitrixreport'=>array('view'),
            'cat1'=>array('view'),//
            'cat2'=>array('view'),//
            'cat5'=>array('view'),//
            'site2'=>array('view'),//
            'site3'=>array('view'),//
            'site4'=>array('view'),//
            'site5'=>array('view'), //
            'site8'=>array('view'),//
            'site9'=>array('view'),//
            'site10'=>array('view'),//
            'site11'=>array('view'),//
            'site12'=>array('view'),//
            'site13'=>array('view'),//
            'site14'=>array('view'),//
        ),
        'manager2' => array(
            'bitrixreport'=>array('view'),
            'cat1'=>array('view'),//
            'cat2'=>array('view'),//
            'cat4'=>array('view'),//
            'cat5'=>array('view'),//
            'site2'=>array('view'),//
            'site3'=>array('view'),//
            'site4'=>array('view'),//
            'site5'=>array('view'), //
            'site7'=>array('view'), //
            'site8'=>array('view'),//
            'site9'=>array('view'),//
            'site10'=>array('view'),//
            'site11'=>array('view'),//
            'site12'=>array('view'),//
            'site13'=>array('view'),//
            'site14'=>array('view'),//
        )
    );
}