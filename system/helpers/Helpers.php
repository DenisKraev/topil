<?php
class Helpers {

    public function redirect($url, $statusCode = 303){
        header('Location: '.$url, true, $statusCode);
        die();
    }

    public function render($content, $data = array()){
        extract($data);
        ob_start();
        include ($content);
        $text = ob_get_contents();
        ob_end_clean();
        return $text;
    }

    public function sortTruncateArr($arr, $item, $truncate, $typeSort = SORT_DESC) {
        foreach ($arr as $key => $row) {
            $sortarr[$key] = $row[$item];
        }
        array_multisort($sortarr, $typeSort, $arr);
        array_splice($arr, $truncate);
        return $arr;
    }

    public function russianMonth($month = ''){
        if($month == ''){
            $month = date("m");
        }
        $date=explode(" ", $month);
        switch ($date[0]){
            case 1: $m='январь'; break;
            case 2: $m='февраль'; break;
            case 3: $m='март'; break;
            case 4: $m='апрель'; break;
            case 5: $m='май'; break;
            case 6: $m='июннь'; break;
            case 7: $m='июль'; break;
            case 8: $m='август'; break;
            case 9: $m='сентябрь'; break;
            case 10: $m='октябрь'; break;
            case 11: $m='ноябрь'; break;
            case 12: $m='декабрь'; break;
        }
        return $m;
    }

    public function strDateToTimestamp($strDate){
        if (preg_match('/^(?P<day>\d+).(?P<month>\d+).(?P<year>\d+)$/', $strDate, $matches)) {
            $timestamp = mktime(0, 0, 0, ($matches['month']), $matches['day'], $matches['year']);
            return $timestamp;
        } else {
            preg_match('/^(?P<day>\d+).(?P<month>\d+).(?P<year>\d+)$/', date("m.d.Y"), $matches);
            $timestamp = mktime(0, 0, 0, ($matches['month']), $matches['day'], $matches['year']);
            return $timestamp;
        }
    }

    public function breadcrumbs($arr, $home = array('url'=>'/', 'name'=>'Главная')){
        if(is_array($arr)){
            rsort($arr);
            return $this->render(ROOT."/system/view/partials/breadcrumbs.php",array('data'=>$arr, 'home'=>$home));
        }else{
            return false;
        }
    }

    public function pagination($count_pages, $active, $count_show_pages, $url, $url_page){

        if ($count_pages > 1) {
            $left = $active - 1;
            $right = $count_pages - $active;
            if ($left < floor($count_show_pages / 2)) $start = 1;
            else $start = $active - floor($count_show_pages / 2);
            $end = $start + $count_show_pages - 1;
            if ($end > $count_pages) {
                $start -= ($end - $count_pages);
                $end = $count_pages;
                if ($start < 1) $start = 1;
            }

            return $this->render(ROOT."/system/view/partials/pagination.php",
                array(
                    'count_pages'=>$count_pages,
                    'active'=>$active,
                    'url'=>$url,
                    'url_page'=>$url_page,
                    'start'=>$start,
                    'end'=>$end,
                ));
        } else {
            return false;
        }

    }

    public function createFile($file, $content, $param = 'w') {
        $file = ROOT.$file;

        $f = fopen($file, $param);
        fwrite($f, $content);
        fclose($f);
        chmod($file, 0777);

        return true;
    }

    public function contentFromFile($file) {
        $file = ROOT.$file;
        $value = file_get_contents($file);
        return $value;
    }

    public function log($level, $content){
        global $config;
        if($config['log']){
            $file_log = ROOT.'/logs/'.$level.'.log';
            $fp = fopen($file_log,"a+");
            fwrite($fp,'|'.time().'| level: '.$level.' | '.$content."\r\n");
            fclose($fp);
            if(chmod($file_log, 0777)){
                return true;
            }else{
                return false;
            }
        } else {
            return false;
        }
    }

}