<?php
require_once (ROOT.'/system/config.php');

try {
    $dbh = new PDO("mysql:host=".$config['db']['host'].";dbname=".$config['db']['name']."", $config['db']['username'], $config['db']['password']);
    $dbh->exec('SET NAMES utf8');
}
catch(PDOException $e) {
    exit($e->getMessage());
}
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

try {
    $dbh2 = new PDO("mysql:host=".$config['db2']['host'].";dbname=".$config['db2']['name']."", $config['db2']['username'], $config['db2']['password']);
    $dbh2->exec('SET NAMES utf8');
}
catch(PDOException $e2) {
    exit($e2->getMessage());
}
$dbh2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);