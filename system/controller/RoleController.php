<?php
require_once (ROOT.'/system/model/Role.php');
class RoleController extends Role{

    public $nameController = "role";
    public $defaultAction = "admin";

    public function actionIndex(){
        $helper = new Helpers;
        $user = new User;

        if(!$user->access('role', 'view')){
            return $helper->render(ROOT."/system/view/admin/error/index.php", array('mass'=>'Доступ запрещен'));
        }

        return 'Индекс';
    }

    public function actionAdmin(){
        $dbh = Core::db();
        $helper = new Helpers;
        $user = new User;

        if(!$user->access('role', 'admin')){
            return $helper->render(ROOT."/system/view/admin/error/index.php", array('mass'=>'Доступ запрещен'));
        }

        if(!isset($_GET['page']) || $_GET['page'] == '') {
            $active = 1;
            $page = 1;
        } else {
            $active = $_GET['page'];
            $page = $_GET['page'];
        }

        $countItems = 20;
        $shift = $countItems * ($page - 1);

        $data = $dbh->query('
          SELECT * FROM role ORDER BY id_role ASC LIMIT '.$shift.', '.$countItems.'')
            ->fetchAll(PDO::FETCH_ASSOC);

        $count = $dbh->query('SELECT COUNT(*) FROM role')->fetch();
        $count = $count[0];

        $count = ceil($count/$countItems);
        $count_show_pages = $count;

        $pagination = $helper->pagination(
            $count,
            $active,
            $count_show_pages,
            $helper->domain.'/admin.php?controller='.$this->nameController.'&action=admin',
            $helper->domain.'/admin.php?controller='.$this->nameController.'&action=admin&page='
        );

        return $helper->render(ROOT."/system/view/admin/role/index.php", array('data'=>$data, 'pagination'=>$pagination));
    }

    public function actionNew(){
        $helper = new Helpers;
        $user = new User;

        if(!$user->access('role', 'create')){
            return $helper->render(ROOT."/system/view/admin/error/index.php", array('mass'=>'Доступ запрещен'));
        }

        if(isset($_POST['submit'])){
            $this->newAdm($_POST);
        } else {
            return $helper->render(ROOT."/system/view/admin/role/form.php");
        }
    }

    public function actionEdit(){
        $helper = new Helpers;
        $user = new User;

        if(!$user->access('role', 'edit')){
            return $helper->render(ROOT."/system/view/admin/error/index.php", array('mass'=>'Доступ запрещен'));
        }

        if(isset($_POST['submit'])){
            $this->editAdm($_POST);
        } else {
            $data = $this->dataAttribute($_GET['id']);
            return $helper->render(ROOT."/system/view/admin/role/form.php", array('data'=>$data, 'edit' => true));
        }
    }

    public function actionDelete() {
        $helper = new Helpers;
        $user = new User;

        if(!$user->access('role', 'delete')){
            return $helper->render(ROOT."/system/view/admin/error/index.php", array('mass'=>'Доступ запрещен'));
        }
        $this->delete($_GET['id']);
    }

}