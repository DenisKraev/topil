<?php
require_once (ROOT.'/system/model/BitrixReport.php');
class BitrixreportController extends BitrixReport {

    public $nameController = "bitrixreport";
    public $defaultAction = "create";

    public function actionCreate(){
        $helper = new Helpers;
        $user = new User;

        if(!$user->access('bitrixreport', 'view')){
            return $helper->render(ROOT."/system/view/admin/error/index.php", array('mass'=>'Доступ запрещен'));
        }

        if(isset($_REQUEST['submit']) || isset($_REQUEST['csv'])){
            $date_start = $_REQUEST['date_start'];
            $date_end = $_REQUEST['date_end'];
        } else {
            $date_start = date('d.m.Y');
            $date_end = date('d.m.Y');
        }

        $reportName = $_GET['id'];

        $report = $this->report(ucfirst($reportName));

        if(isset($_GET['csv'])){
            $fcsv = 'getCSV'.ucfirst($reportName);
            $csv = $this->$fcsv($report, $date_start);
            header('Content-Disposition: attachment; filename='.$reportName.'-'.date('Y-m-d',strtotime($date_start)).'.csv');
            header('Content-Type: application/x-force-download; charset=utf-8; name='.$reportName.'-'.date('Y-m-d',strtotime($date_start)).'.csv');
            echo join(chr(13),$csv);
            die();
        }

        return $helper->render(ROOT."/system/view/bitrixreport/".$reportName.".php", array('report'=>$report, 'date_start'=>$date_start, 'date_end'=>$date_end));
    }

    public function getCSVMinimarket($report, $date_start){

        $yandexArr = array('count'=>0, 'lead'=>0);
        $googleArr = array('count'=>0, 'lead'=>0);
        $areaArr = array(
            array('name'=>'Директ', 'alias'=>'yandex'),
            array('name'=>'AdWords', 'alias'=>'google'),
        );
        foreach($report as $k=>$v){
            foreach($areaArr as $areaItem){
                if(strripos($k, $areaItem['alias']) !== false){
                    $selectArr = $areaItem['alias'].'Arr';
                    foreach($v as $item){
                        if(isset($item['count'])){
                            ${$selectArr}['count'] = ${$selectArr}['count'] + $item['count'];
                        }
                        if(isset($item['lead'])){
                            ${$selectArr}['lead'] = ${$selectArr}['lead'] + $item['lead'];
                        }
                    }
                }
            }
        }

        $csv[]=';Сводная таблица по интернет-маркетингу';
        $csv[]='Дата;Итого вложения;Итого выручка;Количество переходов/стоимость перехода;Количество заявок/стоимость заявки;Стоимость оплаты;Конверсия продукта;Маржа';
        $arr = $report['yandex_il_stat'];
        krsort($arr);
        foreach($arr as $k=>$v){

            $count = 0;
            foreach($report as $item){
                $count = $count+$item[$k]['count'];
            }

            $lead = 0;
            foreach($report as $item){
                $lead = $lead+$item[$k]['lead'];
            }

            $csv[]=$k.';;;'.$count.';'.$lead.';';
        }

        foreach($areaArr as $k=>$v){
            $selectArr = $v['alias'].'Arr';

            $csv[]=';;';
            $csv[]=';;';

            $csv[]=';Директ;';
            $csv[]='Вложения;Переходы;Стоимость перехода;Заявки;Стоимость заявки;Конверсия;Оплаты;Выручка';

            $csv[]=';'.${$selectArr}['count'].';;'.${$selectArr}['lead'].';';
        }
        return $csv;
    }

    public function getCSVDaily($data, $date_start){
        $csv[]=date('Y-m-d',strtotime($date_start)).';Заявки;Звонки;Встречи;Карточка клиента;Счетов;Сумма выставленных счетов;Сумма послуплений;';
        foreach($data as $k=>&$v){
            if(!$v["NAME"])continue;
            $_LEAD_NUM+=$v['LEAD_NUM'];
            $_COMMUNICATIONS_PHONE+=$v['COMMUNICATIONS']['2'];
            $_COMMUNICATIONS_MEET+=$v['COMMUNICATIONS']['1'];
            $_COMMUNICATIONS_EMAIL+=$v['COMMUNICATIONS']['4'];
            $_BILL_COUNT+=$v['BILL_COUNT'];
            $_BILL_SUMM+=$v['BILL_SUMM'];
            $_BILL_PAYED_SUMM+=$v['BILL_PAYED_SUMM'];
            $csv[]=$v["NAME"].' '.$v["LAST_NAME"].';'.$v['LEAD_NUM'].';'.$v['COMMUNICATIONS']['2'].';'.$v['COMMUNICATIONS']['1'].';;'.$v['BILL_COUNT'].';'.$v['BILL_SUMM'].';'.$v['BILL_PAYED_SUMM'].';';
        }
        $csv[]='Итого;'.$_LEAD_NUM.';'.$_COMMUNICATIONS_PHONE.';'.$_COMMUNICATIONS_MEET.';'.$_BILL_COUNT.';'.$_BILL_SUMM.';'.$_BILL_PAYED_SUMM.';';

        return $csv;
    }

    public function getCSVLeadstime($data, $date_start){
        $csv[]='№;Ответственный;Дата создания;Дата распределения;Дата изменения;Время реакции менеджера;ID лид';
        $i=1;
        foreach($data as $k=>&$v){
            $csv[]=$i++.';'.$v["name"].';'.$v['date_create'].';'.$v['date_distribution'].';'.$v['date_change'].';'.$v['reaction_time'].';'.$v['id_lead'].';';
        }
        return $csv;
    }

    public function getCSVMissedcalls($data, $date_start){
        $csv[]='№;Телефон;Дата;Время;Имя';
        $i=1;
        foreach($data as $k=>&$v){
            $csv[]=$i++.';'.$v["phone"].';'.$v['date'].';'.$v['time'].';'.$v['name'].';';
        }
        return $csv;
    }

    public function getCSVPhoner($data, $date_start){
        $csv[]=date('Y-m-d',strtotime($date_start)).';Кол-во лидов создано;Кол-во лидов изменено;Кол-во исходящих писем;Кол-во > 1 мин.;Время > 1;Кол-во > 2 мин.;Время > 2;Кол-во всех;Время всех;Пропущено;';
        foreach($data as $k=>&$v){
            if(!$v["NAME"])continue;
            $_LEAD_CR_NUM+=$v['LEAD_CR_NUM'];
            $_LEAD_NUM+=$v['LEAD_NUM'];
            $_EMAIL_COUNT+=$v['EMAIL_COUNT'];
            $_CALL_COUNT1+=$v['CALL_COUNT1'];
            $_CALL_SUMM1+=$v['CALL_SUMM1'];
            $_CALL_COUNT+=$v['CALL_COUNT'];
            $_CALL_SUMM+=$v['CALL_SUMM'];
            $_CALL_ALL_COUNT+=$v['CALL_ALL_COUNT'];
            $_CALL_ALL_SUMM+=$v['CALL_ALL_SUMM'];
            $_MISS_COUNT+=$v['MISS_COUNT'];
            $csv[]=$v["NAME"].' '.$v["LAST_NAME"].';'.$v['LEAD_CR_NUM'].';'.$v['LEAD_NUM'].';'.$v['EMAIL_COUNT'].';'.$v['CALL_COUNT1'].';'.$v['CALL_SUMM1'].';'.$v['CALL_COUNT'].';'.$v['CALL_SUMM'].';'.$v['CALL_ALL_COUNT'].';'.$v['CALL_ALL_SUMM'].';'.$v['MISS_COUNT'].';';
        }
        $csv[]='Итого;'.$_LEAD_CR_NUM.';'.$_LEAD_NUM.';'.$_EMAIL_COUNT.';'.$_CALL_COUNT1.';'.$_CALL_SUMM1.';'.$_CALL_COUNT.';'.$_CALL_SUMM.';'.$_CALL_ALL_COUNT.';'.$_CALL_ALL_SUMM.';'.$_MISS_COUNT.';';
        return $csv;
    }

}