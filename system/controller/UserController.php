<?php
require_once (ROOT.'/system/model/User.php');
class UserController extends User {

    public $nameController = "user";
    public $defaultAction = "admin";

    public function actionIndex(){
        $helper = new Helpers;

        if(!$this->access('user', 'view')){
            return $helper->render(ROOT."/system/view/admin/error/index.php", array('mass'=>'Доступ запрещен'));
        }

        return 'Индекс';
    }

    public function actionAdmin(){
        $dbh = Core::db();
        $helper = new Helpers;

        if(!$this->access('user', 'admin')){
            return $helper->render(ROOT."/system/view/admin/error/index.php", array('mass'=>'Доступ запрещен'));
        }

        if(!isset($_GET['page']) || $_GET['page'] == '') {
            $active = 1;
            $page = 1;
        } else {
            $active = $_GET['page'];
            $page = $_GET['page'];
        }

        $countItems = 20;
        $shift = $countItems * ($page - 1);

        $data = $dbh->query('
          SELECT * FROM user
          LEFT JOIN role ON user.id_role = role.id_role
          ORDER BY id_user ASC LIMIT '.$shift.', '.$countItems.'')
            ->fetchAll(PDO::FETCH_ASSOC);

        $count = $dbh->query('SELECT COUNT(*) FROM user')->fetch();
        $count = $count[0];

        $count = ceil($count/$countItems);
        $count_show_pages = $count;

        $pagination = $helper->pagination(
            $count,
            $active,
            $count_show_pages,
            $helper->domain.'/admin.php?controller='.$this->nameController.'&action=admin',
            $helper->domain.'/admin.php?controller='.$this->nameController.'&action=admin&page='
        );

        return $helper->render(ROOT."/system/view/admin/user/index.php", array('data'=>$data, 'pagination'=>$pagination));
    }

    public function actionNew(){
        $dbh = Core::db();
        $helper = new Helpers;

        if(!$this->access('user', 'create')){
            return $helper->render(ROOT."/system/view/admin/error/index.php", array('mass'=>'Доступ запрещен'));
        }

        if(isset($_POST['submit'])){
            $new = $this->newAdm($_POST);
            if(is_array($new) && in_array('err',$new)){
                $role = $dbh->query('SELECT * FROM role')->fetchAll(PDO::FETCH_ASSOC);
                return $helper->render(ROOT."/system/view/admin/user/form.php", array('role'=>$role, 'error'=>$new['mess']));
            }
        } else {
            $role = $dbh->query('SELECT * FROM role')->fetchAll(PDO::FETCH_ASSOC);

            return $helper->render(ROOT."/system/view/admin/user/form.php", array('role'=>$role));
        }
    }

    public function actionEdit(){
        $dbh = Core::db();
        $helper = new Helpers;

        if(!$this->access('user', 'edit')){
            return $helper->render(ROOT."/system/view/admin/error/index.php", array('mass'=>'Доступ запрещен'));
        }

        if(isset($_POST['submit'])){
            $edit = $this->editAdm($_POST);
            if(is_array($edit) && in_array('err',$edit)){
                $role = $dbh->query('SELECT * FROM role')->fetchAll(PDO::FETCH_ASSOC);
                return $helper->render(ROOT."/system/view/admin/user/form.php", array('data'=>$edit['attribute'], 'role'=>$role, 'error'=>$edit['mess'], 'edit' => true));
            }
        } else {
            $data = $this->dataAttribute($_GET['id']);
            $role = $dbh->query('SELECT * FROM role')->fetchAll(PDO::FETCH_ASSOC);

            return $helper->render(ROOT."/system/view/admin/user/form.php", array('data'=>$data, 'role'=>$role, 'edit' => true));
        }
    }

    public function actionRegistration(){
        $helper = new Helpers();
        $error = array();
        if($this->isGuest()){
            session_start();
            require_once(ROOT."/system/lib/simple-php-captcha/simple-php-captcha.php");
            if($_POST && !empty($_POST)){
                $data = array('login'=>$_POST['login'], 'password'=>$_POST['password'], 'name'=>$_POST['name'], 'code'=>$_POST['code'], 'captcha_verify'=>$_SESSION['captcha']['code']);
                $data = $this->reg($data);
                if($data){
                    $_SESSION['captcha'] = array();
                    $_SESSION['captcha'] = simple_php_captcha();
                    $error = $data['mess'];
                }
            }else{
                $_SESSION['captcha'] = array();
                $_SESSION['captcha'] = simple_php_captcha();
            }
            return $helper->render(ROOT."/system/view/user/registration.php", array('error'=>$error));
        }else{
            $helper->redirect('/');
        }
    }

    public function actionLogin(){
        $helper = new Helpers();
        $error = array();
        if($this->isGuest()){
            if($_POST && !empty($_POST)){
                $data = $this->auth($_POST['login'],$_POST['password']);
                if($data){
                    $error = $data['mess'];
                }
            }
            return $helper->render(ROOT."/system/view/user/login.php", array('error'=>$error));
        } else {
            $helper->redirect('/');
        }
    }

    public function actionLogout(){
        $helper = new Helpers();
        setcookie("user_id",'', time() - 9999999);
        setcookie("user_id",'', time() - 9999999, "/");
        $helper->redirect('/');
    }

    public function actionDelete() {
        $helper = new Helpers;

        if(!$this->access('user', 'delete')){
            return $helper->render(ROOT."/system/view/admin/error/index.php", array('mass'=>'Доступ запрещен'));
        }
        $this->delete($_GET['id']);
    }

}