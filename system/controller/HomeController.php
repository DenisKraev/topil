<?php
require_once (ROOT.'/system/model/User.php');
require_once (ROOT.'/system/model/Site.php');
class HomeController {

    public $nameController = "home";
    public $defaultAction = "index";

    public function actionIndex(){
        $helper = new Helpers;
        $user = new User;

        if($user->isGuest()){
            $helper->redirect('/user/login/');
        }else{
            $site = new Site;
            if(isset($_GET['cat'])){
                $data = $site->getChildrenSites($_GET['cat']);
                $ancestors = $site->getAncestors($_GET['cat']);
                $breadcrumbs = $helper->breadcrumbs($ancestors);
            } else {
                $data = $site->getRootSites();
            }

            return $helper->render(ROOT."/system/view/home/index.php",array('user'=>$user, 'breadcrumbs'=>$breadcrumbs, 'sites'=>$data, 'site'=>$site));
        }
    }

    public function actionRedirect(){
        $helper = new Helpers;
        $helper->redirect($_GET['url']);
    }

}