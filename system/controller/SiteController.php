<?php
require_once (ROOT.'/system/model/Site.php');
class SiteController extends Site{

    public $nameController = "site";
    public $defaultAction = "admin";

    public function actionIndex(){
        $helper = new Helpers;
        $user = new User;

        if(!$user->access('site', 'view')){
            return $helper->render(ROOT."/system/view/admin/error/index.php", array('mass'=>'Доступ запрещен'));
        }

        return 'Индекс';
    }

    public function actionAdmin(){
        $helper = new Helpers;
        $user = new User;

        if(!$user->access('site', 'admin')){
            return $helper->render(ROOT."/system/view/admin/error/index.php", array('mass'=>'Доступ запрещен'));
        }

        if(isset($_GET['cat'])){
            $data = $this->getChildrenSites($_GET['cat']);
            $parent = $this->getParentId($_GET['cat']);
        } else {
            $data = $this->getRootSites();
        }

        $site = new Site();
        return $helper->render(ROOT."/system/view/admin/site/index.php", array('data'=>$data, 'site'=>$site, 'parent'=>$parent));
    }

    public function actionNew(){
        $helper = new Helpers;
        $user = new User;

        if(!$user->access('role', 'create')){
            return $helper->render(ROOT."/system/view/admin/error/index.php", array('mass'=>'Доступ запрещен'));
        }

        if(isset($_POST['submit'])){
            $this->newAdm($_POST);
        } else {
            $data = $this->dataAttribute($_GET['id']);
            $treeSites = $this->getTreeSites();
            return $helper->render(ROOT."/system/view/admin/site/form.php", array('data'=>$data, 'treeSites'=>$treeSites));
        }
    }

    public function actionEdit(){
        $helper = new Helpers;
        $user = new User;

        if(!$user->access('role', 'edit')){
            return $helper->render(ROOT."/system/view/admin/error/index.php", array('mass'=>'Доступ запрещен'));
        }

        if(isset($_POST['submit'])||isset($_POST['submitstay'])){
            $edit = $this->editAdm($_POST);
            if(is_array($edit) && (in_array('err',$edit) || in_array('submitstay',$edit))){
                $data = $edit['attribute'];
                $error = $edit['mess'];
            }
        } else {
            $data = $this->dataAttribute($_GET['id']);
        }
        $treeSites = $this->getTreeSites();
        $parentId = $this->getParentId($_GET['id']);
        $thumb = $this->thumb($_GET['id'], '_100');
        return $helper->render(ROOT."/system/view/admin/site/form.php", array('thumb'=>$thumb, 'data'=>$data, 'treeSites'=>$treeSites, 'error'=>$error, 'parentId'=>$parentId, 'edit' => true));

    }

    public function actionSort() {
        $dbh = Core::db();

        $res = true;
        foreach($_POST['data'] as $sort=>$id){;

            $arrData = array(
                array($sort, $id)
            );

            try {
                $sth = $dbh->prepare("UPDATE site SET sortable=? WHERE id_site=? ");
                foreach ($arrData as $value) {
                    $sth->execute($value);
                }
            }
            catch(PDOException $e) {
                $res = false;
                print_r($e);
            }
        }
        if($res){
            echo json_encode(array('serv'=>'Порядок сортировки изменени'));
        }
    }

    public function actionDelete() {
        $helper = new Helpers;
        $user = new User;

        if(!$user->access('role', 'delete')){
            return $helper->render(ROOT."/system/view/admin/error/index.php", array('mass'=>'Доступ запрещен'));
        }
        $this->delete($_GET['id']);
    }

}