<?php
ini_set('date.timezone', 'Etc/GMT');
//error_reporting(0);
$config = array(
    'defaultController'=>'Home',
    'defaultAdminController'=>'User',
    'db' => array(
        'host' => '127.0.0.1',
        'name' => 'top',
        'username' =>  'root',
        'password' => '',
        'charset' => 'utf8',
    ),
    'db2' => array(
        'host' => '37.139.47.244',
        'name' => 'lifeprints_production',
        'username' =>  'lifeprints',
        'password' => '3a68d248fab61dbf',
        'charset' => 'utf8',
    ),
    'originImageParams'=>array(
        'file_new_name_body' => 'origin',
        'image_x' => 1920,
        'image_ratio_y' => true,
    ),
    'log'=>true,
);