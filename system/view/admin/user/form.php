<div class="panel panel-default">
    <div class="panel-heading clearfix">
        <h2 class="pull-left"><?php if(isset($edit)) { echo 'Редактирование пользователя';} else {echo 'Новый пользователь';}?></h2>
    </div>
    <div class="panel-body">
        <form action="/admin/user<?php if(isset($edit)){echo '/edit/'.$data['id_user'].'/';} else {echo '/new/';}?>" method="post" class="form-horizontal">
            <div class="form-group">
                <div class="col-md-3">
                    <label class="control-label">Роль</label>
                </div>
                <div class="col-md-9">
                    <select class="form-control" name="id_role">
                        <?php foreach($role as $item){ ?>
                            <option value="<?php echo $item['id_role']; ?>" <?php if($item['id_role'] == $data['id_role'] && isset($edit)){echo 'selected';} ?>><?php echo $item['name_role']; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-3">
                    <label class="control-label">Имя</label>
                </div>
                <div class="col-md-9">
                    <input class="form-control" type="text" name="name" value="<?php if(isset($edit)){ echo $data['name_user'];} ?>">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-3">
                    <label class="control-label">Логин</label>
                </div>
                <div class="col-md-9">
                    <input class="form-control" type="text" name="login" value="<?php if(isset($edit)){ echo $data['login_user'];} ?>">
                    <div class="error-mes"><?php echo $error['login']; ?></div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-3">
                    <label class="control-label"><?php if(isset($edit)){ echo 'Новый пароль';}else{echo 'Пароль';} ?></label>
                </div>
                <div class="col-md-9">
                    <input class="form-control" type="text" name="password" value="">
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-3">
                    <label class="control-label">Активирован</label>
                </div>

                <div class="col-md-9">
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-default <?php if($data['active_user'] == 1 && isset($edit)){echo 'active';} if(!isset($edit)){echo 'active';} ?>">
                            <input type="radio" name="active" id="option1" autocomplete="off" value="1" <?php if($data['active_user'] == 1 && isset($edit)){echo 'checked';} if(!isset($edit)){echo 'checked';} ?>>Да
                        </label>
                        <label class="btn btn-default <?php if($data['active_user'] == 0 && isset($edit)){echo 'active';}?>">
                            <input type="radio" name="active" id="option2" autocomplete="off"  value="0" <?php if($data['active_user'] == 0 && isset($edit)){echo 'checked';} ?>>Нет
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-10">
                    <button type="submit" name="submit" class="btn btn-primary"><?php if(isset($edit)){echo 'Редактировать';}else{echo 'Создать';} ?></button>
                    <a href="/admin/user/admin/" class="btn btn-default">Отмена</a>
                </div>
            </div>
        </form>
    </div>
</div>