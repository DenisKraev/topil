<div class="panel panel-default">
    <div class="panel-heading clearfix">
        <h2 class="pull-left">Пользователи</h2>
        <div class="pull-right"><a href="/admin/user/new/" class="btn btn-primary" role="button">Создать</a></div>
    </div>
    <div class="panel-body">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>id</th>
                <th>Логин</th>
                <th>Имя</th>
                <th>Роль</th>
                <th class="center-text">Активен</th>
                <th class="action">Действие</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($data as $i => $item){ ?>
            <tr>
                <td><?php echo $item['id_user']; ?></td>
                <td><?php echo $item['login_user']; ?></td>
                <td><?php echo $item['name_user']; ?></td>
                <td><?php echo $item['name_role']; ?></td>
                <td class="center-text"><?php echo $item['active_user']?'<i class="glyphicon glyphicon-ok green"></i>':'<i class="glyphicon glyphicon-remove red"></i>'; ?></td>
                <td class="action">
                    <a href="/admin/user/edit/<?php echo $item['id_user']; ?>/" title="Редактировать" class="pencil glyphicon glyphicon-pencil"></a>
                    <a href="/admin/user/delete/<?php echo $item['id_user']; ?>/" title="Удалить" class="delete glyphicon glyphicon-trash js-delete-item"></a>
                </td>
            </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<?php echo $pagination; ?>