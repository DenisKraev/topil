<div class="panel panel-default">
    <div class="panel-heading clearfix">
        <div class="pull-left">
            <h2>Сайты</h2>
        </div>
        <div class="pull-right">
            <?php if(isset($parent)){ ?>
                <?php $parent_url = '/admin/site/admin/cat/'.$parent.'/'; ?>
                <?php if($parent == 0){ $parent_url = '/admin/site/admin/';} ?>
                <a href="<?php echo $parent_url; ?>" class="btn btn-default" role="button">Вверх</a>
            <?php } ?>
            <a href="/admin/site/new/" class="btn btn-primary" role="button">Создать</a>
        </div>
    </div>
    <div class="panel-body">
        <table class="table table-striped sorted-table">
            <thead>
                <tr>
                    <th>id</th>
                    <th></th>
                    <th>Название</th>
                    <th>Правило</th>
                    <th>Картинка</th>
                    <th>Ссылка</th>
                    <th class="action">Действие</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($data as $i => $item){ ?>
                    <tr id="<?php echo $item['id'];?>">
                        <td><?php echo $item['id']; ?></td>
                        <td class="sort"><span class="sort-handle glyphicon glyphicon-sort"></span></td>
                        <td><?php echo $item['name']; ?></td>
                        <td><?php echo $item['rule']; ?></td>
                        <td>
                            <?php $thumb = $site->thumb($item['id'],'_100'); ?>
                            <?php if($thumb){ ?>
                                <div class="img ib"><img src="/content/<?php echo $thumb; ?>" width="40"></div>
                            <?php } ?>
                        </td>
                        <td class="over-text"><?php echo $item['url']; ?></td>
                        <td class="action">
                            <?php if($item['count']){ ?>
                                <a href="/admin/site/admin/cat/<?php echo $item['id']; ?>/" title="Вложения" class="folder glyphicon glyphicon-folder-close"><span class="attachments"><?php echo $item['count']; ?></span></a>
                            <?php } else { ?>
                                <a href="#" title="Пусто" class="folder-empty glyphicon glyphicon-folder-close"></a>
                            <?php } ?>
                            <a href="/admin/site/edit/<?php echo $item['id']; ?>/" title="Редактировать" class="pensil glyphicon glyphicon-pencil"></a>
                            <?php if($item['count']){ ?>
                                <a href="#" title="Удалeние не возможно, есть вложения" class="delete-no glyphicon glyphicon-trash"></a>
                            <?php } else { ?>
                                <a href="/admin/site/delete/<?php echo $item['id']; ?>/" title="Удалить" class="delete glyphicon glyphicon-trash js-delete-item"></a>
                            <?php } ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>