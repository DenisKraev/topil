<?php
function renderItems($treeSites, $parentId, $tire = ''){
    $html = '';
    $tire2 = $tire.'--->';
    foreach($treeSites as $item){
        if($item['id'] == $_GET['id']){
            continue;
        }
        $selected = $parentId == $item['id']?'selected':'';
        $html .= '<option value="'.$item['id'].'" '.$selected.'>';
        $html .= $tire2.$item['name'];
        if(isset($item['children'])){
            $html .= renderItems($item['children'], $parentId, $tire2);
        }
        $html .= '</option>';
    }
    return $html;
}
?>

<div class="panel panel-default">
    <div class="panel-heading clearfix">
        <h2 class="pull-left"><?php if(isset($edit)) {echo 'Редактирование сайта';} else {echo 'Новый сайт';}?></h2>
    </div>
    <div class="panel-body">
        <form enctype="multipart/form-data" action="/admin/site<?php if(isset($edit)){echo '/edit/'.$data['id_site'].'/';} else {echo '/new/';}?>" method="post" class="form-horizontal">
            <div class="form-group">
                <div class="col-md-3">
                    <label class="control-label">Категория</label>
                </div>
                <div class="col-md-9">
                    <select class="form-control" name="id_cat">
                        <option value="0" <?php echo $parentId == 0?'selected':'';?>>Корень</option>
                        <?php echo renderItems($treeSites,$parentId); ?>
                    </select>
                    <div class="error-mes"><?php echo $error['id_cat']; ?></div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-3">
                    <label class="control-label">Ссылка</label>
                </div>
                <div class="col-md-9">
                    <input class="form-control" type="text" name="url_site" value="<?php if(isset($edit)){ echo $data['url_site'];} ?>">
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-3">
                    <label class="control-label">Картинка</label>
                </div>
                <div class="col-md-9">
                    <?php if($thumb){ ?>
                        <div class="form-image">
                            <img src="/content<?php echo $thumb;?>" width="100">
                        </div>
                        <div><input type="checkbox" name="del_image" value="1"> <span class="ib">Удалить картинку</span></div>
                    <?php } else { ?>
                        <div class="form-image">
                            <input type="file" class="form-control" name="image" value="">
                        </div>
                    <?php } ?>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-3">
                    <label class="control-label">Название</label>
                </div>
                <div class="col-md-9">
                    <input class="form-control" type="text" name="name" value="<?php if(isset($edit)){ echo $data['name_site'];} ?>">
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-3">
                    <label class="control-label">Правило доступа</label>
                </div>
                <div class="col-md-9">
                    <input class="form-control" type="text" name="rule" value="<?php if(isset($edit)){ echo $data['rule'];} ?>">
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-3">
                    <label class="control-label">CSS класс</label>
                </div>
                <div class="col-md-9">
                    <input class="form-control" type="text" name="css_class" value="<?php if(isset($edit)){ echo $data['css_class'];} ?>">
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-3">
                    <label class="control-label">Действие</label>
                </div>
                <div class="col-md-9">
                    <select class="form-control" name="action">
                        <option value="1" <?php echo $data['action'] == 1?'selected':'';?>>Ссылка на вложения</option>
                        <option value="2" <?php echo $data['action'] == 2?'selected':'';?>>Внешняя ссылка</option>
                    </select>
                    <div class="error-mes"><?php echo $error['id_cat']; ?></div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-3">
                    <label class="control-label">В модальном окне</label>
                </div>
                <div class="col-md-9">
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-default <?php if($data['modal'] == 1 && isset($edit)){echo 'active';} if(!isset($edit)){echo 'active';} ?>">
                            <input type="radio" name="modal" id="option1" autocomplete="off" value="1" <?php if($data['modal'] == 1 && isset($edit)){echo 'checked';} if(!isset($edit)){echo 'checked';} ?>>Да
                        </label>
                        <label class="btn btn-default <?php if($data['modal'] == 0 && isset($edit)){echo 'active';}?>">
                            <input type="radio" name="modal" id="option2" autocomplete="off"  value="0" <?php if($data['modal'] == 0 && isset($edit)){echo 'checked';} ?>>Нет
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-10">
                    <?php if(isset($edit)){ ?><button type="submit" name="submitstay" class="btn btn-success">Редактировать</button><?php } ?>
                    <button type="submit" name="submit" class="btn btn-primary"><?php if(isset($edit)){echo 'Редактировать и выйти';}else{echo 'Создать';} ?></button>
                    <?php if($data['parent_id_site']){ $outurl = '/admin/site/admin/cat/'.$data['parent_id_site'].'/'; }else{$outurl = '/admin/site/admin/';} ?>
                    <a href="<?php echo $outurl; ?>" class="btn btn-default">Отмена</a>
                </div>
            </div>
        </form>
    </div>
</div>