<div class="panel panel-default">
    <div class="panel-heading clearfix">
        <h2 class="pull-left"><?php if(isset($edit)) {echo 'Редактирование роли';} else {echo 'Новая роль';}?></h2>
    </div>
    <div class="panel-body">
        <form action="/admin/role<?php if(isset($edit)){echo '/edit/'.$data['id_role'].'/';} else {echo '/new/';}?>" method="post" class="form-horizontal">
            <div class="form-group">
                <div class="col-md-3">
                    <label class="control-label">Название</label>
                </div>
                <div class="col-md-9">
                    <input class="form-control" type="text" name="name" value="<?php if(isset($edit)){ echo $data['name_role'];} ?>">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-10">
                    <button type="submit" name="submit" class="btn btn-primary"><?php if(isset($edit)){echo 'Редактировать';}else{echo 'Создать';} ?></button>
                    <a href="/admin/role/admin/" class="btn btn-default">Отмена</a>
                </div>
            </div>
        </form>
    </div>
</div>