<div class="panel panel-default">
    <div class="panel-heading clearfix">
        <h2 class="pull-left">Роли</h2>
        <div class="pull-right"><a href="/admin/role/new/" class="btn btn-primary" role="button">Создать</a></div>
    </div>
    <div class="panel-body">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>id</th>
                <th>Название</th>
                <th class="action">Действие</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($data as $i => $item){ ?>
            <tr>
                <td><?php echo $item['id_role']; ?></td>
                <td><?php echo $item['name_role']; ?></td>
                <td class="action">
                    <a href="/admin/role/edit/<?php echo $item['id_role']; ?>/" title="Редактировать" class="pencil glyphicon glyphicon-pencil"></a>
                    <a href="/admin/role/delete/<?php echo $item['id_role']; ?>/" title="Удалить" class="delete glyphicon glyphicon-trash js-delete-item"></a>
                </td>
            </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<?php echo $pagination; ?>