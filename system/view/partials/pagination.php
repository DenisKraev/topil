<ul class="pagination">
    <?php if ($active != 1) { ?>
        <li><a href="<?php if ($active == 2) { echo $url;} else { echo $url_page.($active - 1); } ?>" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
    <?php } ?>
    <?php for ($i = $start; $i <= $end; $i++) { ?>
        <?php if ($i == $active) { ?>
            <li class="active"><a href="#"><?php echo $i; ?> <span class="sr-only">(current)</span></a></li>
        <?php } else { ?>
            <li><a href="<?php if ($i == 1) { echo $url; } else { echo $url_page.$i;} ?>"><?php echo $i; ?></a></li>
        <?php } ?>
    <?php } ?>
    <?php if ($active != $count_pages) { ?>
        <li><a href="<?php echo $url_page.($active + 1)?>" aria-label="Previous"><span aria-hidden="true">&raquo;</span></a></li>
    <?php } ?>
</ul>