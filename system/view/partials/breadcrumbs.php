<ol class="breadcrumb">
    <?php if($home){ ?>
        <li><a href="<?php echo $home['url']; ?>"><?php echo $home['name']; ?></a></li>
    <?php } ?>
    <?php foreach($data as $item){ ?>
        <?php if($item['id'] == $_GET['cat']){ ?>
            <li><?php echo $item['name']; ?></li>
        <?php } else { ?>
            <li><a href="/home/index/cat/<?php echo $item['id']; ?>/"><?php echo $item['name']; ?></a></li>
        <?php } ?>
    <?php } ?>
</ol>