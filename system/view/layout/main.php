<?php
$getUser = $user->getUser();
$userName = $getUser['name_user'];
$role = $getUser['name_role'];
$userId = $getUser['id_user'];
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="/favicon.ico" rel="shortcut icon">
    <link rel="stylesheet" href="/assets/css/lib/bootstrap.yeti.css">
    <link rel="stylesheet" href="/assets/css/lib/datepicker3.css">
    <link rel="stylesheet" href="/assets/css/app.css">
    <script src="/assets/js/jquery-1.11.3.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/bootstrap-datepicker.js"></script>
    <script src="/assets/js/bootstrap-datepicker.ru.js"></script>
    <script src="/assets/js/jquery.form.min.js"></script>
    <script src="/assets/js/app.js"></script>
    <title>Сервисы InfoLife</title>
</head>
<body>
<nav class="navbar navbar-default">
    <div class="site-content">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Сервисы InfoLife</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="/">Главная</a></li>
                <?php if($role == 'admin') {?><li><a href="/admin/">Админка</a></li><?php } ?>
            </ul>
            <?php if(!$user->isGuest()) { ?>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/admin/user/edit/<?php echo $userId; ?>/"><?php echo $userName; ?></a></li>
                    <li>
                        <a href="/user/logout/">Выход</a>
                    </li>
                </ul>
            <?php } ?>
        </div>
    </div>
</nav>
<div class="site-content">
    <?php echo $data; ?>
</div>
</body>
</html>