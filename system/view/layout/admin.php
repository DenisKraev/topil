<?php
$getUser = $user->getUser();
$userName = $getUser['name_user'];
$userId = $getUser['id_user'];
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="/favicon.ico" rel="shortcut icon">
    <link rel="stylesheet" href="/assets/css/lib/bootstrap.yeti.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/app.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/app_admin.css">
    <script src="/assets/js/jquery-1.11.3.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/jquery-ui.min.js"></script>
    <script src="/assets/js/app_admin.js"></script>
    <title>Сервисы InfoLife</title>
</head>
<body>

    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Административная панель</a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="/admin/">Главная</a></li>
                    <li><a href="/">Сайт</a></li>
                </ul>
                <?php if(!$user->isGuest()) {?>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/admin/user/edit/<?php echo $userId; ?>/"><?php echo $userName; ?></a></li>
                        <li>
                            <a href="/user/logout/">Выход</a>
                        </li>
                    </ul>
                <?php } ?>
            </div>
        </div>
    </nav>

    <div class="container">
        <?php if($getUser['id_role'] == 1) { ?>
            <div class="row row-offcanvas row-offcanvas-right">

                <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
                    <div class="list-group">
                        <a href="/admin/user/admin/" class="list-group-item <?php if($_GET['controller'] == 'user'){echo 'active';}?>">Пользователи</a>
                        <a href="/admin/role/admin/" class="list-group-item <?php if($_GET['controller'] == 'role'){echo 'active';}?>">Роли</a>
                        <a href="/admin/site/admin/" class="list-group-item <?php if($_GET['controller'] == 'site'){echo 'active';}?>">Сайты</a>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-9">
                    <?php echo $data; ?>
                </div>

            </div>
        <?php } else {echo 'Нет доступа';} ?>
    </div>

</body>
</html>