<form action="/index.php?controller=bitrixreport&action=create&id=leadstime" method="post" class="form-report">
    <div class="form-group clearfix">
        <div class="col-md-3"><label class="control-label">Дата</label></div>
        <div class="col-md-9">
            <div class="input-daterange input-group" id="datepicker">
                <span class="input-group-addon">с</span>
                <input type="text" class="input-sm form-control" name="date_start" value="<?php echo $date_start; ?>" />
                <span class="input-group-addon">по</span>
                <input type="text" class="input-sm form-control" name="date_end" value="<?php echo $date_end; ?>"/>
            </div>
        </div>
    </div>

    <div class="form-group clearfix text-center">
        <button type="submit" name="submit" class="btn btn-primary reboot">Обновить</button>
    </div>
</form>

<table class="tbl_new table table-bordered table-striped" cellpadding=0 cellspacing=0>
    <thead>
        <tr>
            <th>№</th>
            <th>Ответственный</th>
            <th>Дата создания</th>
            <th>Дата распределения</th>
            <th>Дата изменения</th>
            <th>Время реакции менеджера</th>
            <th>Ссылка на лид</th>
        </tr>
    </thead>
    <tbody>
        <?php $idn=1; foreach($report as $k=>$v){ ?>
            <tr>
                <td><?php echo $idn++; ?></td>
                <td><?php echo $v['name']; ?></td>
                <td><?php echo $v['date_create']; ?></td>
                <td><?php echo $v['date_distribution']; ?></td>
                <td><?php echo $v['date_change']; ?></td>
                <td><?php echo $v['reaction_time']; ?></td>
                <td><a href="https://infolife.bitrix24.ru/crm/lead/show/<?php echo $v['id_lead']; ?>/">Ссылка на лид</a></td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<a href="/index.php?controller=bitrixreport&action=create&id=leadstime&date_start=<?=$date_start;?>&date_end=<?=$date_end;?>&csv" class="download-csv">Скачать Excel</a>