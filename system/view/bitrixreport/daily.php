<form action="/index.php?controller=bitrixreport&action=create&id=daily" method="post" class="form-report">
    <div class="form-group clearfix">
        <div class="col-md-3"><label class="control-label">Дата</label></div>
        <div class="col-md-9">
            <div class="input-daterange input-group" id="datepicker">
                <span class="input-group-addon">с</span>
                <input type="text" class="input-sm form-control" name="date_start" value="<?php echo $date_start; ?>" />
                <span class="input-group-addon">по</span>
                <input type="text" class="input-sm form-control" name="date_end" value="<?php echo $date_end; ?>"/>
            </div>
        </div>
    </div>

    <div class="form-group clearfix text-center">
        <button type="submit" name="submit" class="btn btn-primary reboot">Обновить</button>
    </div>
</form>

<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th><?php echo $date_start; ?></th>
            <th>Заявки</th>
            <th>Звонки</th>
            <th>Встречи</th>
            <th>Карточка клиента</th>
            <th>Счетов</th>
            <th>Сумма выставленных счетов</th>
            <th>Сумма послуплений</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($report as $k=>&$v){
            if(!$v["NAME"])continue;
            $_LEAD_NUM+=$v['LEAD_NUM'];
            $_COMMUNICATIONS_PHONE+=$v['COMMUNICATIONS']['2'];
            $_COMMUNICATIONS_MEET+=$v['COMMUNICATIONS']['1'];
            $_COMMUNICATIONS_EMAIL+=$v['COMMUNICATIONS']['4'];
            $_BILL_COUNT+=$v['BILL_COUNT'];
            $_BILL_SUMM+=$v['BILL_SUMM'];
            $_BILL_PAYED_SUMM+=$v['BILL_PAYED_SUMM']; ?>
            <tr>
                <td><?php echo $v["NAME"].' '.$v["LAST_NAME"]; ?></td>
                <td><?php echo $v['LEAD_NUM']; ?></td>
                <td><?php echo $v['COMMUNICATIONS']['2']; ?></td>
                <td><?php echo $v['COMMUNICATIONS']['1']; ?></td>
                <td><?php echo $v['COMMUNICATIONS']['4']; ?></td>
                <td><?php echo $v['BILL_COUNT']; ?></td>
                <td><?php echo $v['BILL_SUMM']; ?></td>
                <td><?php echo $v['BILL_PAYED_SUMM']; ?></td>
            </tr>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <td>Итого</td>
            <td><?php echo $_LEAD_NUM; ?></td>
            <td><?php echo $_COMMUNICATIONS_PHONE; ?></td>
            <td><?php echo $_COMMUNICATIONS_MEET; ?></td>
            <td><?php echo $_COMMUNICATIONS_EMAIL; ?></td>
            <td><?php echo $_BILL_COUNT; ?></td>
            <td><?php echo $_BILL_SUMM; ?></td>
            <td><?php echo $_BILL_PAYED_SUMM; ?></td>
        </tr>
    </tfoot>
</table>
<a href="/index.php?controller=bitrixreport&action=create&id=daily&date_start=<?=$date_start;?>&date_end=<?=$date_end;?>&csv" class="download-csv">Скачать Excel</a>