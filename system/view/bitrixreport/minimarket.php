<?php
$yandexArr = array('count'=>0, 'lead'=>0);
$googleArr = array('count'=>0, 'lead'=>0);
$areaArr = array(
    array('name'=>'Директ', 'alias'=>'yandex'),
    array('name'=>'AdWords', 'alias'=>'google'),
);
foreach($report as $k=>$v){
    foreach($areaArr as $areaItem){
        if(strripos($k, $areaItem['alias']) !== false){
            $selectArr = $areaItem['alias'].'Arr';
            foreach($v as $item){
                if(isset($item['count'])){
                    ${$selectArr}['count'] = ${$selectArr}['count'] + $item['count'];
                }
                if(isset($item['lead'])){
                    ${$selectArr}['lead'] = ${$selectArr}['lead'] + $item['lead'];
                }
            }
        }
    }
}
?>

<form action="/index.php?controller=bitrixreport&action=create&id=minimarket" method="post" class="form-report">
    <div class="form-group clearfix">
        <div class="col-md-3"><label class="control-label">Дата</label></div>
        <div class="col-md-9">
            <div class="input-daterange input-group" id="datepicker">
                <span class="input-group-addon">с</span>
                <input type="text" class="input-sm form-control" name="date_start" value="<?php echo $date_start; ?>" />
                <span class="input-group-addon">по</span>
                <input type="text" class="input-sm form-control" name="date_end" value="<?php echo $date_end; ?>"/>
            </div>
        </div>
    </div>

    <div class="form-group clearfix text-center">
        <button type="submit" name="submit" class="btn btn-primary reboot">Обновить</button>
    </div>
</form>

<table class="tbl_new table table-bordered table-striped">
    <thead>
        <tr>
            <th></th>
            <th colspan=7 align=center>Сводная таблица по интернет-маркетингу</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Дата</td>
            <td>Итого вложения</td>
            <td>Итого выручка</td>
            <td>Количество переходов/<br>стоимость перехода</td>
            <td>Количество заявок/<br>стоимость заявки</td>
            <td>Стоимость оплаты</td>
            <td>Конверсия продукта</td>
            <td>Маржа</td>
        </tr>
        <?php $arr = $report['yandex_il_stat']; krsort($arr); foreach($arr as $k=>$v){ ?>
            <tr>
                <td><?php echo $k;?></td>
                <td></td>
                <td></td>
                <td>
                    <?php
                        $count = 0;
                        foreach($report as $item){
                            $count = $count+$item[$k]['count'];
                        }
                        echo $count;
                    ?>
                </td>
                <td>
                    <?php
                        $lead = 0;
                        foreach($report as $item){
                            $lead = $lead+$item[$k]['lead'];
                        }
                        echo $lead;
                    ?>
                </td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        <?php } ?>
    </tbody>
</table>

<?php foreach($areaArr as $k=>$v){ ?>

    <?php $selectArr = $v['alias'].'Arr'; ?>

    <table class="tbl_new table table-bordered table-striped">
        <thead>
            <tr>
                <th></th>
                <th colspan=8 align=center ><?php echo $v['name'] ?></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Вложения</td>
                <td>Переходы</td>
                <td>Стоимость перехода</td>
                <td>Заявки</td>
                <td>Стоимость заявки</td>
                <td>Конверсия</td>
                <td>Оплаты</td>
                <td>Выручка</td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo ${$selectArr}['count']; ?></td>
                <td></td>
                <td><?php echo ${$selectArr}['lead']; ?></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
    </table>

<?php } ?>
<a href="/index.php?controller=bitrixreport&action=create&id=minimarket&date_start=<?=$date_start;?>&date_end=<?=$date_end;?>&csv" class="download-csv">Скачать Excel</a>