<form action="/index.php?controller=bitrixreport&action=create&id=missedcalls" method="post" class="form-report">
    <div class="form-group clearfix">
        <div class="col-md-3"><label class="control-label">Дата</label></div>
        <div class="col-md-9">
            <div class="input-daterange input-group" id="datepicker">
                <span class="input-group-addon">с</span>
                <input type="text" class="input-sm form-control" name="date_start" value="<?php echo $date_start; ?>" />
                <span class="input-group-addon">по</span>
                <input type="text" class="input-sm form-control" name="date_end" value="<?php echo $date_end; ?>"/>
            </div>
        </div>
    </div>

    <div class="form-group clearfix text-center">
        <button type="submit" name="submit" class="btn btn-primary reboot">Обновить</button>
    </div>
</form>

<table class="tbl_new table table-bordered table-striped" cellpadding=0 cellspacing=0>
    <thead>
    <tr>
        <th>№</th>
        <th>Телефон</th>
        <th>Дата</th>
        <th>Время</th>
        <th>Имя</th>
    </tr>
    </thead>
    <tbody>
        <?php $idn=1; foreach($report as $k=>$v){ ?>
            <tr>
                <td><?php echo $idn++; ?></td>
                <td><?php echo $v['phone']; ?></td>
                <td><?php echo $v['date']; ?></td>
                <td><?php echo $v['time']; ?></td>
                <td><?php echo $v['name']; ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<a href="/index.php?controller=bitrixreport&action=create&id=missedcalls&date_start=<?=$date_start;?>&date_end=<?=$date_end;?>&csv" class="download-csv">Скачать Excel</a>