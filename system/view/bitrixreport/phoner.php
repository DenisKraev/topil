<form action="/index.php?controller=bitrixreport&action=create&id=phoner" method="post" class="form-report">
    <div class="form-group clearfix">
        <div class="col-md-3"><label class="control-label">Дата</label></div>
        <div class="col-md-9">
            <div class="input-daterange input-group" id="datepicker">
                <span class="input-group-addon">с</span>
                <input type="text" class="input-sm form-control" name="date_start" value="<?php echo $date_start; ?>" />
                <span class="input-group-addon">по</span>
                <input type="text" class="input-sm form-control" name="date_end" value="<?php echo $date_end; ?>"/>
            </div>
        </div>
    </div>

    <div class="form-group clearfix text-center">
        <button type="submit" name="submit" class="btn btn-primary reboot">Обновить</button>
    </div>
</form>

<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th><?php echo $date_start; ?></th>
            <th>Кол-во лидов создано</th>
            <th>Кол-во лидов изменено</th>
            <th>Кол-во исходящих писем</th>
            <th>Кол-во > 1 мин.</th>
            <th>Время > 1</th>
            <th>Кол-во > 2 мин.</th>
            <th>Время > 2</th>
            <th>Кол-во всех</th>
            <th>Время всех</th>
            <th>Пропущено</th>
        </tr>
    </thead>
    <tbody>
        <?php
            foreach($report as $k=>&$v){
                if(!$v["NAME"])continue;
                $_LEAD_CR_NUM+=$v['LEAD_CR_NUM'];
                $_LEAD_NUM+=$v['LEAD_NUM'];
                $_EMAIL_COUNT+=$v['EMAIL_COUNT'];
                $_CALL_COUNT1+=$v['CALL_COUNT1'];
                $_CALL_SUMM1+=$v['CALL_SUMM1'];
                $_CALL_COUNT+=$v['CALL_COUNT'];
                $_CALL_SUMM+=$v['CALL_SUMM'];
                $_CALL_ALL_COUNT+=$v['CALL_ALL_COUNT'];
                $_CALL_ALL_SUMM+=$v['CALL_ALL_SUMM'];
                $_MISS_COUNT+=$v['MISS_COUNT']; ?>
                <tr>
                    <td><?php echo $v["NAME"].' '.$v["LAST_NAME"]; ?></td>
                    <td><?php echo $v['LEAD_CR_NUM']; ?></td>
                    <td><?php echo $v['LEAD_NUM']; ?></td>
                    <td><?php echo $v['EMAIL_COUNT']; ?></td>
                    <td><?php echo $v['CALL_COUNT1']; ?></td>
                    <td><?php echo $v['CALL_SUMM1']; ?></td>
                    <td><?php echo $v['CALL_COUNT']; ?></td>
                    <td><?php echo $v['CALL_SUMM']; ?></td>
                    <td><?php echo $v['CALL_ALL_COUNT']; ?></td>
                    <td><?php echo $v['CALL_ALL_SUMM']; ?></td>
                    <td><?php echo $v['MISS_COUNT']; ?></td>

                </tr>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <td>Итого</td>
            <td><?php echo $_LEAD_CR_NUM; ?></td>
            <td><?php echo $_LEAD_NUM; ?></td>
            <td><?php echo $_EMAIL_COUNT; ?></td>
            <td><?php echo $_CALL_COUNT1; ?></td>
            <td><?php echo $_CALL_SUMM1; ?></td>
            <td><?php echo $_CALL_COUNT; ?></td>
            <td><?php echo $_CALL_SUMM; ?></td>
            <td><?php echo $_CALL_ALL_COUNT; ?></td>
            <td><?php echo $_CALL_ALL_SUMM; ?></td>
            <td><?php echo $_MISS_COUNT; ?></td>
        </tr>
    </tfoot>
</table>
<a href="/index.php?controller=bitrixreport&action=create&id=phoner&date_start=<?=$date_start;?>&date_end=<?=$date_end;?>&csv" class="download-csv">Скачать Excel</a>