<form class="form-signin" action="/user/login/" method="post">
    <fieldset>
        <legend>Вход</legend>
        <div class="form-group <?php if($error['all']){echo 'has-error';} ?>">
            <div class="error-mes"><?php echo $error['all']; ?></div>
        </div>
        <div class="form-group <?php if($error['login']){echo 'has-error';} ?>">
            <input type="text" class="form-control input-lg" name="login" placeholder="Логин">
            <div class="error-mes"><?php echo $error['login']; ?></div>
        </div>
        <div class="form-group <?php if($error['password']){echo 'has-error';} ?>">
            <input type="password" class="form-control input-lg" name="password" placeholder="Пароль">
            <div class="error-mes"><?php echo $error['password']; ?></div>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-lg btn-primary btn-block" value="Войти">
        </div>
        <div class="form-group text-center">Если не зарегистрированы <a href="/user/registration/">Регистрация</a></div>
    </fieldset>
</form>