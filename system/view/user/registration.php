<form class="form-signin" action="/user/registration/" method="post">
    <fieldset>
        <legend>Регистрация</legend>
        <div class="form-group <?php if($error['all']){echo 'has-error';} ?>">
            <div class="error-mes"><?php echo $error['all']; ?></div>
        </div>
        <div class="form-group <?php if($error['login']){echo 'has-error';} ?>">
            <input type="text" class="form-control input-lg" name="login" placeholder="Логин">
            <div class="error-mes"><?php echo $error['login']; ?></div>
        </div>
        <div class="form-group <?php if($error['password']){echo 'has-error';} ?>">
            <input type="password" class="form-control input-lg" name="password" placeholder="Пароль">
            <div class="error-mes"><?php echo $error['password']; ?></div>
        </div>
        <div class="form-group <?php if($error['name']){echo 'has-error';} ?>">
            <input type="text" class="form-control input-lg" name="name" placeholder="Имя">
            <div class="error-mes"><?php echo $error['name']; ?></div>
        </div>
        <div class="captcha form-group row <?php if($error['code']){echo 'has-error';} ?>">
            <div class="col-lg-7">
                <input type="text" class="form-control ib input-lg" name="code" placeholder="Код">
                <div class="error-mes"><?php echo $error['code']; ?></div>
            </div>
            <div class="col-lg-5">
                <?php echo '<img src="'.$_SESSION['captcha']['image_src'].'" alt="CAPTCHA">'; ?>
            </div>
        </div>
        <div class="form-group"><input type="submit" class="btn btn-lg btn-primary btn-block" value="Регистрация"></div>
    </fieldset>
    <div class="form-group text-center">Если зарегистрированы <a href="/user/login/">Войти</a></div>
</form>