<?php echo $breadcrumbs; ?>

<div class="site-list">
    <?php foreach($sites as $item){ ?>
        <?php if(!$user->access($item['rule'], 'view')){continue;} ?>
        <div class="ib site-item <?php echo $item['css_class']; ?>">
            <h4 class="title"><?php echo $item['name']; ?></h4>
            <?php
                if($item['action']==1){
                    $url = '/home/index/cat/'.$item['id'].'/';
                    $target = '_self';
                    $linkText = 'Открыть категорию';
                }
                if($item['action'] == 2){
                    $url = $item['url'];
                    $target = '_blank';
                    $linkText = 'Перейти на сервис';
                }
                $thumb = '<img src="/content'.$site->thumb($item["id"],"_210").'">';
            ?>
            <a class="img-box <?php echo $item['modal']?'modal-button':''; ?>" data-remote="false" <?php echo $item['modal']?'data-toggle=modal':''; ?> data-title="<?php echo $item['name']; ?>" href="<?php echo $url; ?>" target="<?php echo $target; ?>"><?php echo $thumb; ?></a>
            <p><a class="btn btn-default btn-xs <?php echo $item['modal']?'modal-button':''; ?>"  <?php echo $item['modal']?'data-toggle=modal':''; ?> data-title="<?php echo $item['name']; ?>"  href="<?php echo $url; ?>" target="<?php echo $target; ?>" ><?php echo $linkText; ?></a></p>
        </div>
    <?php } ?>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>